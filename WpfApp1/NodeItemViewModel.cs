﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver.Enums;
using LeducHoldemSolver;

namespace WpfApp1
{
	public class NodeItemViewModel : ViewModelBase
	{
		private static readonly Card[] Deck = new[]
		{
			new Card(Ranks.Queen, Suits.Hearts),
			new Card(Ranks.King, Suits.Hearts),
			new Card(Ranks.Ace, Suits.Hearts),
			new Card(Ranks.Queen, Suits.Spades),
			new Card(Ranks.King, Suits.Spades),
			new Card(Ranks.Ace, Suits.Spades)
		};

		private readonly string _state;
		private readonly LeducNode _leducNode;

		private string _title;
		private string _stats;
		private string _strategy;
		private string _regretSum;

		public LeducNode LeducNode => _leducNode;

		public string Title
		{
			get => _title;
			private set
			{
				_title = value;
				OnPropertyChanged();
			}
		}

		public string Stats
		{
			get => _stats;
			private set
			{
				_stats = value;
				OnPropertyChanged();
			}
		}

//		public string Strategy
//		{
//			get => _strategy;
//			private set
//			{
//				_strategy = value;
//				OnPropertyChanged();
//			}
//		}

//		public string RegretSum
//		{
//			get => _regretSum;
//			private set
//			{
//				_regretSum = value;
//				OnPropertyChanged();
//			}
//		}

		public ObservableCollection<NodeItemViewModel> Items { get; private set; }
		public ObservableCollection<Tuple<Card, string, string, string>> Strategy { get; private set; }

		public NodeItemViewModel(LeducNode node)
		{
			_state = node.State.ToString();
			Items = new ObservableCollection<NodeItemViewModel>();
			Strategy = new ObservableCollection<Tuple<Card, string, string, string>>();

			_leducNode = node;

			UpdateLabels();
		}

		

		public void UpdateLabels()
		{
			Title = $"{(string.IsNullOrWhiteSpace(_state) ? "Empty" : _state)}";
//			Stats = $"P:{_leducNode.RealizationWeight} | U:{_leducNode.LastUtil}";

			Strategy.Clear();

			if (_leducNode.Values != null)
			{
				foreach (var card in Deck)
				{
					Strategy.Add(new Tuple<Card, string, string, string>(
						card,
						$"[{(_leducNode.Values.ContainsKey(card) ? string.Join(", ", _leducNode.Values[card].Strategy) : string.Empty)}]",
						$"[{(_leducNode.Values.ContainsKey(card) ? string.Join(", ", _leducNode.Values[card].RegretSum) : string.Empty)}]",
						$"[{(_leducNode.Values.ContainsKey(card) && _leducNode.Values[card].Utility != null ? string.Join(", ", _leducNode.Values[card].Utility.Select(u => ($"{u.Key}: {u.Value}"))) : string.Empty)}]"
					));

					
				}
			}

//			if (_leducNode.Strategy != null)
//			{
//				Strategy = $"[{string.Join(", ", _leducNode.Strategy)}]";
//			}
//
//			if (_leducNode.RegretSum != null)
//			{
//				RegretSum = $"[{string.Join(", ", _leducNode.RegretSum)}]";
//			}
		}

		public void RecursiveUpdateLabels()
		{
			UpdateLabels();
			foreach (var child in Items)
			{
				child.RecursiveUpdateLabels();
			}
		}

		public void Expand()
		{
			if (_leducNode.Children != null)
			{
				RecursiveClear();
			}

			_leducNode.Expand();

			foreach (var child in _leducNode.Children)
			{
				Items.Add(new NodeItemViewModel(child));
			}
		}

		public void RecursiveExpand()
		{
			Expand();

			foreach (var child in Items)
			{
				child.RecursiveExpand();
			}
		}

		public void RecursiveClear()
		{
			foreach (var item in Items)
			{
				item.RecursiveClear();
			}

			_leducNode.Children.Clear();
			Items.Clear();
		}
	}
}
