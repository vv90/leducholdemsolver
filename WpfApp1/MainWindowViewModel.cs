﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CfrSolver.Enums;
using LeducHoldemSolver;

namespace WpfApp1
{
	public class MainWindowViewModel : ViewModelBase
	{
		private static readonly Random Rng = new Random();
		private static readonly Card[] Deck = new[]
		{
			new Card(Ranks.Queen, Suits.Hearts),
			new Card(Ranks.King, Suits.Hearts),
			new Card(Ranks.Ace, Suits.Hearts),
			new Card(Ranks.Queen, Suits.Spades),
			new Card(Ranks.King, Suits.Spades),
			new Card(Ranks.Ace, Suits.Spades)
		};

		private int _totalIterations;
		private double _totalUtil;
		private double _avgUtil;
		private double _lastUtil;
		

		public int TotalIterations
		{
			get => _totalIterations;
			set
			{
				_totalIterations = value;
				OnPropertyChanged();
			}
		}

		public double TotalUtil
		{
			get => _totalUtil;
			set
			{
				_totalUtil = value;
				OnPropertyChanged();
			}
		}

		public double AvgUtil
		{
			get => _avgUtil;
			set
			{
				_avgUtil = value;
				OnPropertyChanged();
			}
		}

		public double LastUtil
		{
			get => _lastUtil;
			set
			{
				_lastUtil = value;
				OnPropertyChanged();
			}
		}

		public ICommand AdvanceOneCommand { get; set; }
		public ICommand AdvanceTenCommand { get; set; }
		public ICommand AdvanceHundredCommand { get; set; }
		public ICommand ResetCommand { get; set; }

		public ObservableCollection<NodeItemViewModel> RootItems { get; private set; }
		public NodeItemViewModel Root { get; private set; }

		public MainWindowViewModel()
		{
			RootItems = new ObservableCollection<NodeItemViewModel>();

			AdvanceOneCommand = new RelayCommand(o => Advance(1));
			AdvanceTenCommand = new RelayCommand(o => Advance(10));
			AdvanceHundredCommand = new RelayCommand(o => Advance(100));
			ResetCommand = new RelayCommand(o => Reset());

			Reset();
		}

		private void Advance(int iterations)
		{
			var cards = new Card[2];
			for (var i = 0; i < iterations; i++)
			{
				ShuffleCards();
				cards[0] = Deck[0];
				cards[1] = Deck[1];

				LastUtil = Root.LeducNode.Cfr(cards, 1, 1);
				TotalUtil += LastUtil;
				TotalIterations += 1;
				AvgUtil = TotalUtil / TotalIterations;
			}

			Root.RecursiveUpdateLabels();
		}

		private void Reset()
		{
			var rootNode = new LeducNode(new LeducState());

			Root = new NodeItemViewModel(rootNode);

			Root.RecursiveExpand();

			RootItems.Clear();
			RootItems.Add(Root);

			TotalIterations = 0;
			TotalUtil = 0;
			AvgUtil = 0;
			LastUtil = 0;
		}

		private void ShuffleCards()
		{
			for (var c1 = Deck.Length - 1; c1 > 0; c1--)
			{
				var c2 = Rng.Next(c1 + 1);
				var tmp = Deck[c1];
				Deck[c1] = Deck[c2];
				Deck[c2] = tmp;
			}
		}
	}
}
