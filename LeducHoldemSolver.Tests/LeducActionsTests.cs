﻿using System;
using CfrSolver.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeducHoldemSolver.Tests
{
	[TestClass]
	public class LeducActionsTests
	{
		[TestMethod]
		public void TestPlayerActionConstructor()
		{
			var action = new PlayerAction(Actions.Call, 12345);

			Assert.AreEqual(Actions.Call, action.Action);
			Assert.AreEqual(12345, action.Amount);
		}

		[TestMethod]
		public void TestLeducActionsIndexer()
		{
			var actions = new[] {
				new PlayerAction(Actions.Raise, 100),
				new PlayerAction(Actions.Raise, 200)
			};

			var leducActions = new LeducActions(actions);

			Assert.AreEqual(actions[0], leducActions[0]);
			Assert.AreEqual(actions[1], leducActions[1]);
		}

		[TestMethod]
		public void TestLeducActionsEquality()
		{
			var leducActions = new LeducActions(new[] {
				new PlayerAction(Actions.Raise, 100),
				new PlayerAction(Actions.Raise, 200),
				new PlayerAction(Actions.Raise, 400),
				new PlayerAction(Actions.Raise, 800),
				new PlayerAction(Actions.Raise, 1600),
				new PlayerAction(Actions.Raise, 3200),
				new PlayerAction(Actions.Raise, 6400),
				new PlayerAction(Actions.Call, 6400)
			});
			var sameLeducActions = new LeducActions(new[] {
				new PlayerAction(Actions.Raise, 100),
				new PlayerAction(Actions.Raise, 200),
				new PlayerAction(Actions.Raise, 400),
				new PlayerAction(Actions.Raise, 800),
				new PlayerAction(Actions.Raise, 1600),
				new PlayerAction(Actions.Raise, 3200),
				new PlayerAction(Actions.Raise, 6400),
				new PlayerAction(Actions.Call, 6400)
			});

			Assert.AreEqual(leducActions, sameLeducActions);
		}

		[TestMethod]
		public void TestLeducActionsEquality_NotEqualForDifferentActions()
		{
			var actions1 = new LeducActions(new[]
			{
				new PlayerAction(Actions.Raise, 300),
				new PlayerAction(Actions.Raise, 500)
			});

			var actions2 = new LeducActions(new[]
			{
				new PlayerAction(Actions.Raise, 300),
				new PlayerAction(Actions.Raise, 300)
			});
		
			Assert.AreNotEqual(actions1, actions2);
		}

		[TestMethod]
		public void TestPlayerActionEquality_DifferentActionsNotEqual()
		{
			var action1 = new PlayerAction(Actions.Raise, 500);

			var action2 = new PlayerAction(Actions.Raise, 300);

			Assert.AreNotEqual(action1, action2);
		}

		[TestMethod]
		public void TestLeducActionsIndexer_ThrowsIndexOutOfRangeException_IfIndexLT0()
		{
			var leducActions = new LeducActions();

			Assert.ThrowsException<IndexOutOfRangeException>(() =>
			{
				var temp = leducActions[-1];
			});
		}

		[TestMethod]
		public void TestLeducActionsIndexer_ThrowsIndexOutOfRangeException_IfIndexGT8()
		{
			var leducActions = new LeducActions();

			Assert.ThrowsException<IndexOutOfRangeException>(() =>
			{
				var temp = leducActions[9];
			});
		}
	}
}
