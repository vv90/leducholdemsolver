﻿using System;
using System.Linq;
using CfrSolver.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace LeducHoldemSolver.Tests
{
	[TestClass]
	public class LeducNodeTests
	{
		private void RecursiveCompareNodes(LeducNode expected, LeducNode actual)
		{
			Assert.AreEqual(expected.State, actual.State);
			CollectionAssert.AreEquivalent(expected.Values.Keys, actual.Values.Keys);

			foreach (var key in expected.Values.Keys)
			{
				CollectionAssert.AreEqual(expected.Values[key].Strategy, actual.Values[key].Strategy);
				CollectionAssert.AreEqual(expected.Values[key].StrategySum, actual.Values[key].StrategySum);
				CollectionAssert.AreEqual(expected.Values[key].RegretSum, actual.Values[key].RegretSum);
			}
			
			Assert.AreEqual(expected.Children.Count, actual.Children.Count);

			for (var i = 0; i < expected.Children.Count; i++)
			{
				RecursiveCompareNodes(expected.Children[i], actual.Children[i]);
			}
		}

		[TestMethod]
		public void TestSerialization()
		{
			var rootNode = new LeducNode(new LeducState());

			foreach (var card1 in Card.EnumerateLeducDeck())
			{
				foreach (var card2 in Card.EnumerateLeducDeck().Where(c => !c.Equals(card1)))
				{
					rootNode.Cfr(new[] {card1, card2}, 1, 1);
				}
			}
			
//			RecursiveExpandNode(rootNode);

			var treeStr = JsonConvert.SerializeObject(rootNode);

			var deserializedNode = JsonConvert.DeserializeObject<LeducNode>(treeStr);

			RecursiveCompareNodes(rootNode, deserializedNode);
		}

		[TestMethod]
		public void TestGetPcsTerminalUtility()
		{
			var deck = Card.EnumerateLeducDeck().ToArray();
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Hearts),
				new[]
				{
					new PlayerAction(Actions.Raise, 600),
					new PlayerAction(Actions.Call, 600)
				});

			var node = new LeducNode(state);
			node.Expand();

//			var util = node.GetPcsTerminalUtility(0, deck);

			Assert.IsTrue(true);
		}
	}
}
