﻿using System;
using System.Collections.Generic;
using CfrSolver.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeducHoldemSolver.Tests
{
	internal class MockLeducPlayer : ILeducPlayer
	{
		private Queue<PlayerAction> _actionQueue;

		public int UtilityGain { get; }

		public MockLeducPlayer(IEnumerable<PlayerAction> actions)
		{
			_actionQueue = new Queue<PlayerAction>(actions);
		}
		
		public void SetCard(Card card)
		{
			
		}

		public void SetState(LeducState state, IEnumerable<PlayerAction> possibleActions)
		{
			if (_actionQueue.Count > 0)
			{
				PlayerActingEvent?.Invoke(this, new PlayerActingEventArgs(_actionQueue.Dequeue()));
			}
		}

		public void SetUtility(int amount)
		{
			
		}

		public event EventHandler<PlayerActingEventArgs> PlayerActingEvent;
	}

	[TestClass]
	public class LeducDealerTests
	{
		[TestMethod]
		public void TestSetState()
		{
//			var state = LeducState.Parse("c100c100Ahc0");
			
			var player0 = new MockLeducPlayer(new []
			{
				new PlayerAction(Actions.Call, 100),
				new PlayerAction(Actions.Call, 0)
			});
			var player1 = new MockLeducPlayer(new []
			{
				new PlayerAction(Actions.Call, 100), 
				new PlayerAction(Actions.Call, 0) 
			});

			var dealer = new LeducDealer(player0, player1);

			dealer.StartGame();
			
			Assert.IsTrue(dealer.IsGameFinished);
		}
	}
}
