﻿using System;
using System.Collections.Generic;
using System.Linq;
using CfrSolver.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LeducHoldemSolver.Tests
{
	public static class LeducStateAssertExtentions
	{
		public static void StatesAreEqual(this CollectionAssert customAssert, ICollection<LeducState> expected,
			ICollection<LeducState> actual)
		{
			if (expected.Count != actual.Count)
			{
				throw new AssertFailedException("Different number of elements.");
			}

			var i = 0;
			foreach (var compare in expected.Zip(actual, (exp, act) => new { exp, act }))
			{
				if (!compare.exp.Equals(compare.act))
				{
					throw new AssertFailedException($"Elements at index {i} do not match. Expected {compare.exp}, got {compare.act}");
				}

				i++;
			}
		}

		public static void StatesAreSame(this CollectionAssert customAssert, ICollection<LeducState> expected,
			ICollection<LeducState> actual)
		{
			var containerList = new List<LeducState>(actual);

			foreach (var exp in expected)
			{
				if (!containerList.Contains(exp))
				{
					throw new AssertFailedException($"{exp} is missing.");
				}

				containerList.Remove(exp);
			}

			if (containerList.Any())
			{
				var unexpected = string.Join(", ", containerList.Select(s => s.ToString()));
				throw new AssertFailedException($"Elements {unexpected} were not expected.");
			}
		}
	}

	[TestClass]
	public class LeducStateTests
	{
		[TestMethod]
		public void TestParse_ParsesCorrectly()
		{
			var state = LeducState.Parse("c100c100Ahc0r400f");

			var expectedState = new LeducState(
				new []
				{
					new PlayerAction(Actions.Call, 100), 
					new PlayerAction(Actions.Call, 100)
				},
				new Card(Ranks.Ace, Suits.Hearts),
				new []
				{
					new PlayerAction(Actions.Call, 0), 
					new PlayerAction(Actions.Raise, 400), 
					new PlayerAction(Actions.Fold, 0)
				});

			Assert.AreEqual(expectedState, state);
		}

		[TestMethod]
		public void TestEquality_SameStatesAreEqual()
		{
			var state1 = new LeducState(
				new[] 
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Spades),
				new[] 
				{
					new PlayerAction(Actions.Raise, 600),
					new PlayerAction(Actions.Call, 600)
				});

			var state2 = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Spades),
				new[]
				{
					new PlayerAction(Actions.Raise, 600),
					new PlayerAction(Actions.Call, 600)
				});

			Assert.AreEqual(state1, state2);
		}

		[TestMethod]
		public void TestEquality_DifferentStatesAreNotEqual()
		{
			var state1 = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Raise, 500)
				});

			var state2 = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Raise, 300)
				});

			Assert.AreNotEqual(state1, state2);
		}
		

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsNullForEmptyState()
		{
			var cards = new[]
			{
				new Card(Ranks.Queen, Suits.Hearts),
				new Card(Ranks.King, Suits.Hearts)
			};

			var state = new LeducState();

			Assert.IsNull(state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsNullAfterFirstBetRound()
		{
			var cards = new[]
			{
				new Card(Ranks.Queen, Suits.Hearts),
				new Card(Ranks.King, Suits.Hearts)
			};

			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				});

			Assert.IsNull(state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsCorrectUtility_CallCallOnFlop()
		{
			var cards = new[]
			{
				new Card(Ranks.Ace, Suits.Spades),
				new Card(Ranks.King, Suits.Hearts)
			};

			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Call, 100),
					new PlayerAction(Actions.Raise, 500),
					new PlayerAction(Actions.Call, 500), 
				},
				new Card(Ranks.Ace, Suits.Hearts),
				new[]
				{
					new PlayerAction(Actions.Call, 0),
					new PlayerAction(Actions.Call, 0)
				});

			Assert.AreEqual(500, state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsCorrectUtility_WhenSecondPlayerFolds()
		{
			var cards = new[]
			{
				new Card(Ranks.Queen, Suits.Hearts),
				new Card(Ranks.King, Suits.Hearts)
			};

			// var state = new LeducState("r2f");
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Fold, 0)
				});

			Assert.AreEqual(100, state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsCorrectUtility_WhenFirstPlayerFolds()
		{
			var cards = new[]
			{
				new Card(Ranks.Queen, Suits.Hearts),
				new Card(Ranks.King, Suits.Hearts)
			};

			// var state = new LeducState("f");
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Fold, 0)
				});

			Assert.AreEqual(100, state.GetTerminalUtility(cards));
		}


		[TestMethod]
		public void TestGetTerminalUtility_ReturnsReturnsCorrectUtility_AfterSecondBetRound_P1HighCard()
		{
			var cards = new[]
			{
				new Card(Ranks.King, Suits.Hearts),
				new Card(Ranks.Queen, Suits.Hearts)
			};

			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Hearts),
				new[]
				{
					new PlayerAction(Actions.Raise, 600),
					new PlayerAction(Actions.Call, 600)
				});

			Assert.AreEqual(900, state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsReturnsCorrectUtility_AfterSecondBetRound_P2HighCard()
		{
			var cards = new[]
			{
				new Card(Ranks.Queen, Suits.Hearts),
				new Card(Ranks.King, Suits.Hearts)
			};
			
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Hearts),
				new[]
				{
					new PlayerAction(Actions.Raise, 600),
					new PlayerAction(Actions.Call, 600)
				});

			Assert.AreEqual(-900, state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsReturnsCorrectUtility_AfterSecondBetRound_P1Pair()
		{
			var cards = new[]
			{
				new Card(Ranks.Ace, Suits.Hearts),
				new Card(Ranks.King, Suits.Hearts)
			};

			//			var state = new LeducState("r1cAsr1c");
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Spades),
				new[]
				{
					new PlayerAction(Actions.Raise, 600),
					new PlayerAction(Actions.Call, 600)
				});

			Assert.AreEqual(900, state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsReturnsCorrectUtility_AfterSecondBetRound_P2Pair()
		{
			var cards = new[]
			{
				new Card(Ranks.King, Suits.Hearts),
				new Card(Ranks.Ace, Suits.Hearts)
			};

			// var state = new LeducState("r1cAsr1c");
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Spades),
				new[]
				{
					new PlayerAction(Actions.Raise, 600),
					new PlayerAction(Actions.Call, 600)
				});

			Assert.AreEqual(-900, state.GetTerminalUtility(cards));
		}

		[TestMethod]
		public void TestGetTerminalUtility_ReturnsCorrectUtility_AllinCallPreflop()
		{
			var cards = new[]
			{
				new Card(Ranks.King, Suits.Hearts),
				new Card(Ranks.Ace, Suits.Hearts)
			};

			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 10000),
					new PlayerAction(Actions.Call, 10000)
				}, new Card(Ranks.Ace, Suits.Spades));

			var util = state.GetTerminalUtility(cards);

			Assert.AreEqual(-10000, util);
		}
		
		[TestMethod]
		public void TestExpand_ExpandStartingState()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState();

			var nextStates = state.Expand().Cast<LeducState>().ToArray();
			var expectedStates = new []
			{
				new LeducState(new [] { new PlayerAction(Actions.Fold, 0) }),
				new LeducState(new [] { new PlayerAction(Actions.Call, 100) }),
				new LeducState(new [] { new PlayerAction(Actions.Raise, 300) }),
				new LeducState(new [] { new PlayerAction(Actions.Raise, 500) })
			};

			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);
		}
		
		[TestMethod]
		public void TestExpand_ExpandRaise()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(new[] {new PlayerAction(Actions.Raise, 300)});

			var nextStates = state.Expand().Cast<LeducState>().ToArray();
			var expectedStates = new[]
			{
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Fold, 0)
					}),
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					}),
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Raise, 500)
					}),
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Raise, 900)
					})
			};


			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);
		}

		[TestMethod]
		public void TestExpand_ExpandRaise_DoesNotGoOverMaxStackSize()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 1000),
					new PlayerAction(Actions.Raise, 8000)
				});

			var nextStates = state.Expand().Cast<LeducState>().ToArray();
			var expectedStates = new[]
			{
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Raise, 1000),
						new PlayerAction(Actions.Raise, 8000),
						new PlayerAction(Actions.Fold, 0)
					}),
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Raise, 1000),
						new PlayerAction(Actions.Raise, 8000),
						new PlayerAction(Actions.Call, 8000)
					}),
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Raise, 1000),
						new PlayerAction(Actions.Raise, 8000),
						new PlayerAction(Actions.Raise, 10000)
					})
			};


			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);
		}

		[TestMethod]
		public void TestExpand_NoRaiseAfterAllin()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 1000),
					new PlayerAction(Actions.Raise, 10000)
				});

			var nextStates = state.Expand().Cast<LeducState>().ToArray();

			var expectedStates = new[]
			{
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 1000),
						new PlayerAction(Actions.Raise, 10000),
						new PlayerAction(Actions.Fold, 0)
					}),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 1000),
						new PlayerAction(Actions.Raise, 10000),
						new PlayerAction(Actions.Call, 10000)
					})
			};

			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);
		}

		[TestMethod]
		public void TestExpand_ExpandRaiseFold()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Fold, 0)
				});

			var nextStates = state.Expand().Cast<LeducState>().ToArray();

			Assert.AreEqual(0, nextStates.Length);
		}

		[TestMethod]
		public void TestExpand_ExpandChanceNode()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				});

			var nextStates = state.Expand().Cast<LeducState>().ToArray();
			var expectedStates = new[]
			{
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Ace, Suits.Hearts)),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Ace, Suits.Spades)),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.King, Suits.Hearts)),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.King, Suits.Spades)),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Queen, Suits.Hearts)),
				new LeducState ( 
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Queen, Suits.Spades))
			};

			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);

		}

		[TestMethod]
		public void TestExpand_DoesNotExpandTerminalState()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Queen, Suits.Spades),
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				});

			var nextStates = state.Expand().Cast<LeducState>();

			Assert.AreEqual(0, nextStates.Count());
		}

		[TestMethod]
		public void TestExpand_ExpandPreflopAllin_DoesNotExandAfterFlop()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 10000),
					new PlayerAction(Actions.Call, 10000)
				}, new Card(Ranks.Ace, Suits.Hearts));

			var nextStates = state.Expand().Cast<LeducState>().ToArray();

			Assert.AreEqual(0, nextStates.Length);
		}

		[TestMethod]
		public void TestExpand_CheckCheck()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Call, 100),
					new PlayerAction(Actions.Call, 100)
				});

			var nextStates = state.Expand().Cast<LeducState>().ToArray();

			Assert.AreEqual(6, nextStates.Length);
		}

		[TestMethod]
		public void TestExpand_Check()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Call, 100)
				});

			var nextStates = state.Expand().Cast<LeducState>().ToArray();

			var expectedStates = new[]
			{
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Call, 100),
						new PlayerAction(Actions.Fold, 0)
					}),
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Call, 100),
						new PlayerAction(Actions.Call, 100)
					}),
				new LeducState( 
					new []
					{
						new PlayerAction(Actions.Call, 100),
						new PlayerAction(Actions.Raise, 300)
					}),
				new LeducState(
					new []
					{
						new PlayerAction(Actions.Call, 100),
						new PlayerAction(Actions.Raise, 500)
					})
			};

			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);
		}

		[TestMethod]
		public void TestExpand_ExpandsFlopCorrectly()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Queen, Suits.Spades)
			);

			var nextStates = state.Expand().Cast<LeducState>().ToArray();

			var expectedStates = new[]
			{
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Queen, Suits.Spades),
					new[]
					{
						new PlayerAction(Actions.Fold, 0)
					}
				),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Queen, Suits.Spades),
					new[]
					{
						new PlayerAction(Actions.Call, 0),
					}
				),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Queen, Suits.Spades),
					new[]
					{
						new PlayerAction(Actions.Raise, 600),
					}
				),
				new LeducState(
					new[]
					{
						new PlayerAction(Actions.Raise, 300),
						new PlayerAction(Actions.Call, 300)
					},
					new Card(Ranks.Queen, Suits.Spades),
					new[]
					{
						new PlayerAction(Actions.Raise, 1200)
					}
				)
			};

			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);
		}

		[TestMethod]
		public void TestExpand_DoesNotAllowMoreThanMaximumNumberOfBetsInARound()
		{
			var betSizings = new[] { 1.0, 2.0 };
			var state = new LeducState(
				Enumerable.Range(1, LeducState.ActionsLength)
					.Select(i => new PlayerAction(Actions.Raise, i * 100))
			);

			var nextStates = state.Expand().Cast<LeducState>().ToArray();

			var expectedStates = new[]
			{
				new LeducState(
					Enumerable.Range(1, LeducState.ActionsLength)
						.Select(i => new PlayerAction(Actions.Raise, i * 100))
						.Concat(new []{ new PlayerAction(Actions.Fold, 0)})
				),
				new LeducState(
					Enumerable.Range(1, LeducState.ActionsLength)
						.Select(i => new PlayerAction(Actions.Raise, i * 100))
						.Concat(new []{ new PlayerAction(Actions.Call, LeducState.ActionsLength * 100) })
				)
			};

			CollectionAssert.That.StatesAreEqual(expectedStates, nextStates);
		}

		[TestMethod]
		public void TestAggregateState_SetsCurrentPlayerCorrectly_Fold()
		{
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Fold, 0)
				}
			);

			var aggregateState = state.GetAggregateState();

			Assert.AreEqual(1, aggregateState.CurrentPlayer);
		}

		[TestMethod]
		public void TestAggregateState_SetsCurrentPlayerCorrectly_Call()
		{
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Call, 100)
				}
			);

			var aggregateState = state.GetAggregateState();

			Assert.AreEqual(1, aggregateState.CurrentPlayer);
		}

		[TestMethod]
		public void TestPartialMatch_ChecksFlop()
		{
			var state = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Spades)
			);

			var state2 = new LeducState(
				new[]
				{
					new PlayerAction(Actions.Raise, 300),
					new PlayerAction(Actions.Call, 300)
				},
				new Card(Ranks.Ace, Suits.Hearts)
			);

			Assert.IsFalse(state.PartialMatch(state2));
		}
	}
}
