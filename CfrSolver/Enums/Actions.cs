﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CfrSolver.Enums
{
	public enum Actions
	{
		Empty,
		Fold,
		Call,
		Raise
	}
}
