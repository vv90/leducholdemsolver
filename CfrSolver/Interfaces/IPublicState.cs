﻿using System;
using System.Collections.Generic;

namespace CfrSolver.Interfaces
{
	public interface IPrivateState : IEquatable<IPrivateState>
	{
//		bool IsValidCombination(IPrivateState other);
	}

	public interface IPublicState<T, G> : IEquatable<IPublicState<T, G>> 
		where G: IGame<T> 
		where T: IPrivateState
	{
		bool IsTerminal();
		bool IsChanceNode();
		int GetUtility(T[] privateStates, int player);
		int CurrentPlayer();
		bool IsValidCombination(params T[] privateStates);
		IEnumerable<T> GetPossiblePrivateStates();
		IEnumerable<IPublicState<T, G>> Expand();
		bool IsReachableFrom(IPublicState<T, G> other);

		string Serialize();
		IPublicState<T, G> Deserialize(string value);
	}
	
}
