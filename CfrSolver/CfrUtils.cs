﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver.Interfaces;

namespace CfrSolver
{
	public class CfrUtils
	{
		public static T[] Shuffle<T>(T[] privateStates) where T: IPrivateState
		{
			var rng = new Random();
			var shuffledStates = privateStates.ToArray();
			for (var c1 = privateStates.Length - 1; c1 > 0; c1--)
			{
				var c2 = rng.Next(c1 + 1);
				var tmp = shuffledStates[c1];
				shuffledStates[c1] = shuffledStates[c2];
				shuffledStates[c2] = tmp;
			}

			return shuffledStates;
		}
	}
}
