﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CfrSolver
{
	public class ProgressUpdateEventArgs : EventArgs
	{
		public double PercentComplete { get; private set; }
		public string AdditionalInfo { get; private set; }

		public ProgressUpdateEventArgs(double percentComplete, string additionalInfo = "")
		{
			PercentComplete = percentComplete;
			AdditionalInfo = additionalInfo;
		}
	}

	public class ProgressTracker
	{
		private int _debounceTime;
		private int _lastIterCount;
		private DateTime? _lastUpdate;

		public event EventHandler<ProgressUpdateEventArgs> ProgressUpdateEvent;
		
		public ProgressTracker(int debounceTime)
		{
			_debounceTime = debounceTime;
		}

		public void UpdateFromIteration(int current, int total)
		{
			var now = DateTime.Now;
			if ((_lastUpdate == null || now.Subtract(_lastUpdate.Value).TotalMilliseconds > _debounceTime) && ProgressUpdateEvent != null)
			{
				var iterPerSec = (1000.0 * (current - _lastIterCount)) / _debounceTime;

				_lastUpdate = now;
				_lastIterCount = current;
				ProgressUpdateEvent(this, new ProgressUpdateEventArgs(100.0 * current / total, $"{iterPerSec} iter/sec"));
			}
		}
	}
}
