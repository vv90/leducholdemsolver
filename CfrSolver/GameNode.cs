﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver.Interfaces;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using Newtonsoft.Json;

namespace CfrSolver
{
	public class GameNodeValues
	{
		[JsonProperty]
		public Vector<double> StrategySum { get; private set; }

		[JsonProperty]
		public Vector<double> RegretSum { get; private set; }

		[JsonProperty]
		public Vector<double> Strategy { get; private set; }

		public GameNodeValues(int length)
		{
			if (length > 0)
			{
				StrategySum = Vector<double>.Build.Dense(length);
				RegretSum = Vector<double>.Build.Dense(length);
				Strategy = Vector<double>.Build.Dense(length);
			}
		}

		public Vector<double> MatchRegrets()
		{
			var normalizingSum = 0.0;
			var strategy = Vector<double>.Build.Dense(RegretSum.Count);

			for (var i = 0; i < RegretSum.Count; i++)
			{
				strategy[i] = Math.Max(RegretSum[i], 0.0);
				normalizingSum += strategy[i];
			}

			for (var i = 0; i < RegretSum.Count; i++)
			{
				strategy[i] = normalizingSum > 0 ? strategy[i] / normalizingSum : 1.0 / RegretSum.Count;
			}

			return strategy;
		}

		public void NormalizeStrategySums()
		{
			if (StrategySum == null)
			{
				return;
			}
			Strategy = StrategySum.Normalize(1);
		}

		public override string ToString()
		{
			return string.Join(", ", StrategySum.Zip(RegretSum, (ss, rs) => new { ss, rs }).Zip(Strategy, (z, s) => $"{z.ss}/{z.rs}/{s}"));
		}
	}
	
	[JsonObject(MemberSerialization.OptIn)]
	public class GameNode<T, G> where G: IGame<T> where T: IPrivateState
	{
		public static GameNode<T, G> TrainCfr(int numIterations, IPublicState<T, G> startingState, Action<double, string> onProgressUpdate = null)
		{
			var rng = new Random();
			var rootNode = new GameNode<T, G>(startingState);
			var progressTracker = onProgressUpdate == null ? null : new ProgressTracker(1000);

			if (progressTracker != null)
			{
				progressTracker.ProgressUpdateEvent +=
					(sender, args) => onProgressUpdate(args.PercentComplete, args.AdditionalInfo);
			}

			for (var i = 0; i < numIterations; i++)
			{
				progressTracker?.UpdateFromIteration(i, numIterations);

				

				
				for (var p = 0; p < 2; p++)
				{
					var possiblePrivateStates = rootNode.State.GetPossiblePrivateStates().ToArray();

					T privateState0 = possiblePrivateStates[rng.Next(possiblePrivateStates.Length)]; ;
					T privateState1;

					do
					{
						privateState1 = possiblePrivateStates[rng.Next(possiblePrivateStates.Length)];
					}
					while (!rootNode.State.IsValidCombination(privateState0, privateState1));

					rootNode.Cfr(p, new[] { privateState0, privateState1 }, Vector<double>.Build.Dense(2, 1.0));
				}
			}

			rootNode.RecursiveNormalizeStrategySums();

			return rootNode;
		}

		public static double CalculateExploitability(GameNode<T, G> rootNode)
		{
			var opponentPrivateStates = rootNode.State.GetPossiblePrivateStates().ToArray();
			var playerPrivateStates = rootNode.State.GetPossiblePrivateStates().ToArray();

			var util = 0.0;
			for (var player = 0; player < 2; player++)
			{
				foreach (var privateState in playerPrivateStates)
				{
					var reachProbabilities = Vector<double>.Build.Dense(
						opponentPrivateStates.Select(s => rootNode.State.IsValidCombination(privateState, s) ? 1.0 : 0.0).ToArray()
					);

					util += rootNode.BestResponseValue(player, privateState, reachProbabilities, opponentPrivateStates) / playerPrivateStates.Length;
				}
			}

			return util / 2;
		}

		public static void Save(GameNode<T, G> rootNode, string file)
		{
			File.WriteAllText(file, JsonConvert.SerializeObject(rootNode, GetJsonConverters()));
		}

		public static GameNode<T, G> Load(string file)
		{
			return JsonConvert.DeserializeObject<GameNode<T, G>>(File.ReadAllText(file), GetJsonConverters());
		}

		[JsonProperty]
		public IPublicState<T, G> State { get; private set; }

		[JsonProperty]
		public Dictionary<T, GameNodeValues> Values { get; private set; }

		[JsonProperty]
		public GameNode<T, G>[] Children { get; private set; }

		public GameNode(IPublicState<T, G> state)
		{
			State = state;
		}

		private void Expand()
		{
			Children = State.Expand().Select(s => new GameNode<T, G>(s)).ToArray();
			Values = State.GetPossiblePrivateStates().ToDictionary(p => p, p => new GameNodeValues(Children.Length));
		}
		
		private void RecursiveNormalizeStrategySums()
		{
			foreach (var child in Children)
			{
				child.RecursiveNormalizeStrategySums();
			}

			foreach (var val in Values)
			{
				val.Value.NormalizeStrategySums();
			}
		}

		public double Cfr(int player, T[] privateStates, Vector<double> reachProbabilities)
		{
			if (Children == null)
			{
				Expand();
			}

			var util = 0.0;

			if (State.IsTerminal())
			{
				return State.GetUtility(privateStates, player);
			}

			if (State.IsChanceNode())
			{
				var validChildren = Children.Where(c => c.State.IsValidCombination(privateStates)).ToArray();
				foreach (var child in validChildren)
				{
					util += child.Cfr(player, privateStates, reachProbabilities) / validChildren.Length;
				}

				return util;
			}

			var strategy = Values[privateStates[State.CurrentPlayer()]].MatchRegrets();
			var childUtils = strategy.MapIndexed((si, s) =>
				Children[si].Cfr(player, privateStates, reachProbabilities.MapIndexed((pi, p) => pi == State.CurrentPlayer() ? p * s : p))
			);

			util = strategy.PointwiseMultiply(childUtils).Sum();

			if (State.CurrentPlayer() == player)
			{
				Values[privateStates[player]].RegretSum.MapIndexedInplace((i, r) => r + childUtils[i] - util);
				Values[privateStates[player]].StrategySum.MapIndexedInplace((i, s) => s + reachProbabilities[player] * strategy[i]);
			}

			return util;
		}

		public double BestResponseValue(int player, T playerPrivateState, Vector<double> reachProbabilities, T[] opponentPrivateStates)
		{
			if (Children.Length == 0)
			{
				var util = reachProbabilities.Select((p, i) =>
				{
					var privateStates = new[] {playerPrivateState, opponentPrivateStates[i]};
					return State.IsValidCombination(privateStates)
						? State.GetUtility(privateStates, player) * reachProbabilities[i]
						: 0;
				}).Sum();

				return util;
			}

			if (State.IsChanceNode())
			{
				return Children.Select(child =>
				{
					var validOpponentPrivateStatesCount = 0;
					var updatedReachProbabilities = reachProbabilities.MapIndexed((i, p) =>
					{
						if (child.State.IsValidCombination(opponentPrivateStates[i]))
						{
							validOpponentPrivateStatesCount += 1;
							return p;
						}
						else
						{
							return 0;
						}
					});
					return child.BestResponseValue(player, playerPrivateState, updatedReachProbabilities, opponentPrivateStates) /
					       validOpponentPrivateStatesCount;
				}).Sum();
			}

			if (State.CurrentPlayer() == player)
			{
				return Children.Max(child =>
				{
					return child.BestResponseValue(player, playerPrivateState, reachProbabilities, opponentPrivateStates);
				});
			}
			else
			{
				return Children.Select((child, childIndex) =>
				{
					var updatedReachProbabilities = reachProbabilities.MapIndexed((i, p) => Values[opponentPrivateStates[i]].Strategy[childIndex] * p);
					return child.BestResponseValue(player, playerPrivateState, updatedReachProbabilities, opponentPrivateStates);
				}).Sum();
			}
		}

//		public GameNodeValues LookupValues(IPrivateState<T> state)
//		{
//
//		}

		public override string ToString()
		{
			return State.ToString();
		}

		private static JsonConverter[] GetJsonConverters()
		{
			return new JsonConverter[]
			{
				new PublicStateJsonConverter<T, G>(),
				new DenseVectorJsonConverter<double>()
			};
		}
	}
}
