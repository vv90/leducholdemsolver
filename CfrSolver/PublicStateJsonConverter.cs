﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver.Interfaces;
using Newtonsoft.Json;

namespace CfrSolver
{
	public class PublicStateJsonConverter<T, G> : JsonConverter<IPublicState<T, G>>
		where T: IPrivateState
		where G: IGame<T>
	{
		public override void WriteJson(JsonWriter writer, IPublicState<T, G> value, JsonSerializer serializer)
		{
			writer.WriteValue(value.Serialize());
		}

		public override IPublicState<T, G> ReadJson(JsonReader reader, Type objectType, IPublicState<T, G> existingValue, bool hasExistingValue,
			JsonSerializer serializer)
		{
			var constructor = objectType.GetConstructor(new[] {typeof(string)});
			if (constructor == null)
			{
				throw new Exception($"Deserialization constructor not implemented. Implement ctor(string) for type {objectType.Name}");
			}

			return (IPublicState<T, G>) constructor.Invoke(new [] {reader.Value});
		}
	}
}
