﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using Newtonsoft.Json;

namespace CfrSolver
{
	public class DenseVectorJsonConverter<T> : JsonConverter<Vector<T>> where T: struct, IEquatable<T>, IFormattable
	{
		public override void WriteJson(JsonWriter writer, Vector<T> value, JsonSerializer serializer)
		{
			writer.WriteValue(JsonConvert.SerializeObject(value.Storage.AsArray()));
		}

		public override Vector<T> ReadJson(JsonReader reader, Type objectType, Vector<T> existingValue, bool hasExistingValue,
			JsonSerializer serializer)
		{
			return Vector<T>.Build.Dense(JsonConvert.DeserializeObject<T[]>((string) reader.Value));
		}
	}
}
