﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Interfaces;
using Dealer.Interfaces;
using KuhnSolver;

namespace Dealer
{
	public class KuhnHumanPlayer : IPlayer<KuhnCard, KuhnPokerGame>
	{
		public int UtilityGain { get; private set; }

		public void SetState(KuhnCard privateState, IPublicState<KuhnCard, KuhnPokerGame> state, Action<IPublicState<KuhnCard, KuhnPokerGame>> selectNextState)
		{
			Console.WriteLine($"{privateState} -> {state}");

			if (state.IsTerminal())
			{
				return;
			}

			var nextStates = state.Expand().ToArray();

			Console.WriteLine(string.Join(", ", nextStates.Select((s, i) => $"{i}: {s.ToString()}")));

			var stateIndex = -1;
			bool readSuccess;
			do
			{
				readSuccess = int.TryParse(Console.ReadLine(), out stateIndex);

			} while (!readSuccess || stateIndex < 0 || stateIndex >= nextStates.Length);

			selectNextState(nextStates[stateIndex]);
		}

		public void SetUtility(int utility, IPublicState<KuhnCard, KuhnPokerGame> state, KuhnCard[] privateStates)
		{
			UtilityGain += utility;
			Console.WriteLine($"[{string.Join(", ", privateStates)}] -> {state}");
			Console.WriteLine($"Utility gain: {utility}. Total: {UtilityGain}");
			Console.WriteLine("------------\n");
		}
	}
}
