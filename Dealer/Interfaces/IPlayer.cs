﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Interfaces;

namespace Dealer.Interfaces
{
	
	public interface IPlayer<T, G>
		where T: IPrivateState
		where G : IGame<T>
	{
//		event EventHandler<PlayerActingEventArgs<T>> PlayerActivgEvent;
		void SetState(T privateState, IPublicState<T, G> state, Action<IPublicState<T, G>> selectNextState);
		void SetUtility(int utility, IPublicState<T, G> state, T[] privateStates);
	}
}
