﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Interfaces;
using Dealer.Interfaces;

namespace Dealer
{
	public class Dealer<T, G> 
		where T: IPrivateState
		where G: IGame<T>
	{
		public IPublicState<T, G> State { get; private set; }
		private KeyValuePair<IPlayer<T, G>, T>[] _players;

		public void Play(IPublicState<T, G> initialState, params IPlayer<T, G>[] players)
		{
			var privateStates = CfrUtils.Shuffle(initialState.GetPossiblePrivateStates().ToArray());
			_players = players.Select((p, i) => new KeyValuePair<IPlayer<T, G>, T>(p, privateStates[i])).ToArray();

			SetState(initialState);

		}

		private void SetState(IPublicState<T, G> state)
		{
			State = state;

			if (!State.IsTerminal())
			{
				var currentPlayer = State.CurrentPlayer();
				_players[currentPlayer].Key.SetState(_players[currentPlayer].Value, State, SetState);
			}
			else
			{
				FinishGame();
			}
		}

		private void FinishGame()
		{
			var privateStates = _players.Select(p => p.Value).ToArray();
			for (var i = 0; i < _players.Length; i++)
			{
				_players[i].Key.SetUtility(State.GetUtility(privateStates, i), State, _players.Select(p => p.Value).ToArray());
			}
		}
	}
}
