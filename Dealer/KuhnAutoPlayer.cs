﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Interfaces;
using Dealer.Interfaces;
using KuhnSolver;

namespace Dealer
{
	public class KuhnAutoPlayer : IPlayer<KuhnCard, KuhnPokerGame>
	{
		private GameNode<KuhnCard, KuhnPokerGame> _rootNode;

		public KuhnAutoPlayer(GameNode<KuhnCard, KuhnPokerGame> rootNode)
		{
			_rootNode = rootNode;
		}

		public void SetState(KuhnCard privateState, IPublicState<KuhnCard, KuhnPokerGame> state, Action<IPublicState<KuhnCard, KuhnPokerGame>> selectNextState)
		{
			throw new NotImplementedException();
		}

		public void SetUtility(int utility, IPublicState<KuhnCard, KuhnPokerGame> state, KuhnCard[] privateStates)
		{
			throw new NotImplementedException();
		}
	}
}
