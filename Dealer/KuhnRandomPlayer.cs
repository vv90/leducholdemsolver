﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Interfaces;
using Dealer.Interfaces;
using KuhnSolver;

namespace Dealer
{
	public class KuhnRandomPlayer : IPlayer<KuhnCard, KuhnPokerGame>
	{
		private readonly Random _rng = new Random();

		public int UtilityGain { get; private set; }

		public void SetState(KuhnCard privateState, IPublicState<KuhnCard, KuhnPokerGame> state, Action<IPublicState<KuhnCard, KuhnPokerGame>> selectNextState)
		{
			if (state.IsTerminal())
			{
				return;
			}

			var nextStates = state.Expand().ToArray();

			selectNextState(nextStates[_rng.Next(nextStates.Length)]);
		}

		public void SetUtility(int utility, IPublicState<KuhnCard, KuhnPokerGame> state, KuhnCard[] privateStates)
		{
			UtilityGain += utility;
		}
	}
}
