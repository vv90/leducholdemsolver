﻿using System;
using CfrSolver.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KuhnSolver.Tests
{
	[TestClass]
	public class KuhnStateTests
	{
		[TestMethod]
		public void TestGetUtility_P0_CallCall_AK()
		{
			var state = new KuhnState(new KuhnActions(new []
			{
				Actions.Call,
				Actions.Call
			}));

			var util = state.GetUtility(new[] {new KuhnCard(Ranks.Ace), new KuhnCard(Ranks.King)}, 0);

			Assert.AreEqual(1, util);
		}

		[TestMethod]
		public void TestGetUtility_P0_CallCall_KA()
		{
			var state = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Call
			}));

			var util = state.GetUtility(new[] { new KuhnCard(Ranks.King), new KuhnCard(Ranks.Ace) }, 0);

			Assert.AreEqual(-1, util);
		}

		[TestMethod]
		public void TestGetUtility_P0_CallRaiseCall_AK()
		{
			var state = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise,
				Actions.Call
			}));

			var util = state.GetUtility(new[] { new KuhnCard(Ranks.Ace), new KuhnCard(Ranks.King) }, 0);

			Assert.AreEqual(2, util);
		}

		[TestMethod]
		public void TestGetUtility_P0_CallRaiseCall_KA()
		{
			var state = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise,
				Actions.Call
			}));

			var util = state.GetUtility(new[] { new KuhnCard(Ranks.King), new KuhnCard(Ranks.Ace) }, 0);

			Assert.AreEqual(-2, util);
		}

		[TestMethod]
		public void TestGetUtility_P1_CallRaiseCall_AK()
		{
			var state = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise,
				Actions.Call
			}));

			var util = state.GetUtility(new[] { new KuhnCard(Ranks.Ace), new KuhnCard(Ranks.King) }, 1);

			Assert.AreEqual(-2, util);
		}

		[TestMethod]
		public void TestIsReachableFrom_RetrunsTrueForSameState()
		{
			var state = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise,
				Actions.Call
			}));

			var other = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise,
				Actions.Call
			}));

			Assert.IsTrue(state.IsReachableFrom(other));
			Assert.IsTrue(other.IsReachableFrom(state));
		}

		[TestMethod]
		public void TestIsReachableFrom_RetrunsTrueForReachableState()
		{
			var state = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise
			}));

			var other = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise,
				Actions.Call
			}));

			Assert.IsTrue(state.IsReachableFrom(other));
		}

		[TestMethod]
		public void TestIsReachableFrom_RetrunsFalseForNotReachableState()
		{
			var state = new KuhnState(new KuhnActions(new[]
			{
				Actions.Call,
				Actions.Raise
			}));

			var other = new KuhnState(new KuhnActions(new[]
			{
				Actions.Raise,
				Actions.Call
			}));

			Assert.IsFalse(state.IsReachableFrom(other));
		}
	}
}
