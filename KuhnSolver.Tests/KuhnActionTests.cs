﻿using System;
using CfrSolver.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KuhnSolver.Tests
{
	[TestClass]
	public class KuhnActionTests
	{
//		[TestMethod]
//		public void TestIndexer()
//		{
//			var actions = new KuhnActions();
//
//			actions[0] = Actions.Fold;
//			actions[1] = Actions.Call;
//			actions[2] = Actions.Raise;
//
//			Assert.AreEqual(Actions.Fold, actions[0]);
//			Assert.AreEqual(Actions.Call, actions[1]);
//			Assert.AreEqual(Actions.Raise, actions[2]);
//		}

		[TestMethod]
		public void TestConstructor_InitializesToEmpty()
		{
			var actions = new KuhnActions();

			Assert.AreEqual(Actions.Empty, actions[0]);
			Assert.AreEqual(Actions.Empty, actions[1]);
			Assert.AreEqual(Actions.Empty, actions[2]);
		}
	}
}
