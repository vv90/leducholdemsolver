﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Enums;
using CfrSolver.Interfaces;

namespace KuhnSolver
{
	
	public class KuhnState : IPublicState<KuhnCard, KuhnPokerGame>
	{
		public static readonly int ActionsLength = 3;

		public static KuhnState Parse(string value)
		{
			return new KuhnState(ParseActions(value));
		}

		private static KuhnActions ParseActions(string value)
		{
			var regex = new Regex($"({string.Join("|", Enum.GetNames(typeof(Actions)))})");

			var matches = regex.Matches(value);
			var actionsList = new List<Actions>();

			foreach (var match in matches)
			{
				if (match is Match m && Enum.TryParse(m.Value, out Actions action))
				{
					actionsList.Add(action);
				}
			}

			return new KuhnActions(actionsList);
		}

		public readonly KuhnActions GameActions;

		public KuhnState()
		{
			
		}

		public KuhnState(string value)
		{
			GameActions = ParseActions(value);
		}

		public KuhnState(KuhnActions actions)
		{
			GameActions = actions;
		}
		
		public bool IsTerminal()
		{
			if (GameActions[0] == Actions.Call && GameActions[1] == Actions.Call ||
			    GameActions[0] == Actions.Raise && GameActions[1] != Actions.Empty ||
			    GameActions[1] == Actions.Raise && GameActions[2] != Actions.Empty)
			{
				return true;
			}
			
			return false;
		}

		public bool IsChanceNode()
		{
			return false;
		}

		public int GetUtility(KuhnCard[] privateStates, int player)
		{
			if (!IsTerminal())
			{
				throw new Exception("Can't get utility. State is not terminal.");
			}

			var bets = new[] {1, 1};

			var currentPlayer = 0;
			int? winner = null;
			for (var i = 0; i < ActionsLength; i++)
			{
				currentPlayer = i % 2;
				if (GameActions[i] == Actions.Raise)
				{
					bets[currentPlayer] = 2;
				}

				if (GameActions[i] == Actions.Fold)
				{
					winner = 1 - currentPlayer;
					break;
				}

				if (GameActions[i] == Actions.Call)
				{
					bets[currentPlayer] = bets[1 - currentPlayer];
				}
			}

			if (!winner.HasValue)
			{
				winner = privateStates[0].Rank > privateStates[1].Rank ? 0 : 1;
			}

			return bets[1 - winner.Value] * (player == winner ? 1 : -1);
		}

//		public int GetUtility(KuhnCard[] privateStates, int player)
//		{
//			return CurrentPlayer() == player ? GetUtility(privateStates) : -GetUtility(privateStates);
//		}

		public int CurrentPlayer()
		{
			var currentPlayer = 0;
			for (var i = 0; i < ActionsLength; i++)
			{
				currentPlayer = i % 2;
				if (GameActions[i] == Actions.Empty)
				{
					break;
				}
			}

			return currentPlayer;
		}

		public bool IsValidCombination(params KuhnCard[] privateStates)
		{
			for (var i = 0; i < privateStates.Length; i++)
			{
				for (var j = i + 1; j < privateStates.Length; j++)
				{
					if (privateStates[i].Equals(privateStates[j]))
					{
						return false;
					}
				}
			}

			return true;
		}

		public IEnumerable<KuhnCard> GetPossiblePrivateStates()
		{
			return KuhnCard.EnumerateDeck();
		}

		public IEnumerable<IPublicState<KuhnCard, KuhnPokerGame>> Expand()
		{
			if (IsTerminal())
			{
				yield break;
			}

			Actions? lastAction = null;

			for (var i = 0; i < ActionsLength; i++)
			{
				if (GameActions[i] == Actions.Empty)
				{
					break;
				}

				lastAction = GameActions[i];
			}

			if (lastAction.HasValue && lastAction.Value == Actions.Raise)
			{
				yield return new KuhnState(GameActions.Add(Actions.Fold));
				yield return new KuhnState(GameActions.Add(Actions.Call));
			}
			else
			{
				yield return new KuhnState(GameActions.Add(Actions.Call));
				yield return new KuhnState(GameActions.Add(Actions.Raise));
			}
		}

		public bool IsReachableFrom(IPublicState<KuhnCard, KuhnPokerGame> other)
		{
			var index = 0;
			while (index < ActionsLength && GameActions[index] != Actions.Empty)
			{
				if (other is KuhnState k && GameActions[index] != k.GameActions[index])
				{
					return false;
				}

				index += 1;
			}

			return true;
		}

		public string Serialize()
		{
			return ToString();
		}

		public IPublicState<KuhnCard, KuhnPokerGame> Deserialize(string value)
		{
			return Parse(value);
		}

		public bool Equals(KuhnState other)
		{
			return GameActions == other?.GameActions;
		}

		public bool Equals(IPublicState<KuhnCard, KuhnPokerGame> other)
		{
			return (other is KuhnState s) && Equals(s);
		}

		public override string ToString()
		{
			var sb = new StringBuilder();

			for (var i = 0; i < ActionsLength; i++)
			{
				if (GameActions[i] == Actions.Empty)
				{
					break;
				}

				sb.Append(GameActions[i]);
			}

			return sb.ToString();
		}
	}
}
