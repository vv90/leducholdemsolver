﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Enums;
using CfrSolver.Interfaces;

namespace KuhnSolver
{
	public struct KuhnCard : IPrivateState, IEquatable<KuhnCard>
	{
		public static IEnumerable<KuhnCard> EnumerateDeck()
		{
			return new[] {new KuhnCard(Ranks.Ace), new KuhnCard(Ranks.King), new KuhnCard(Ranks.Queen)};
		}

		public readonly Ranks Rank;
		

		public KuhnCard(Ranks rank)
		{
			Rank = rank;
		}

		public override bool Equals(object obj)
		{
			return (obj is KuhnCard c) && Equals(c);
		}

		public override int GetHashCode()
		{
			var hash = 17;
			hash = hash * (int) Rank;
			return hash;
		}
		

		public bool Equals(IPrivateState other)
		{
			return (other is KuhnCard c) && Equals(c);
		}

		public bool Equals(KuhnCard other)
		{
			return Rank == other.Rank;
		}

		public override string ToString()
		{
			return Rank.ToString();
		}
	}
}
