﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver.Enums;

namespace KuhnSolver
{
	public struct KuhnActions
	{
		public static bool operator == (KuhnActions first, KuhnActions second)
		{
			return first._data == second._data;
		}

		public static bool operator !=(KuhnActions first, KuhnActions second)
		{
			return first._data != second._data;
		}

		private static uint SetAction(int index, Actions value, uint data)
		{
			if (index > 2 || index < 0)
			{
				throw new IndexOutOfRangeException();
			}

			data |= (uint)((byte)value << index * 8);

			return data;
		}

		private readonly uint _data;

		public KuhnActions(IEnumerable<Actions> actions)
		{
			_data = 0;
			var index = 0;
			foreach (var act in actions)
			{
				if (index >= KuhnState.ActionsLength)
				{
					break;
				}

				_data = SetAction(index++, act, _data);
			}
		}

		private KuhnActions(uint data)
		{
			_data = data;
		}

		public Actions this[int index]
		{
			get
			{
				if (index > 2 || index < 0)
				{
					throw new IndexOutOfRangeException();
				}
				
				return (Actions) ((_data & (0xFF << (index * 8))) >> (index * 8));
			}
		}

		[Pure]
		public KuhnActions Add(Actions action)
		{
			var result = _data;

			for (var i = 0; i < KuhnState.ActionsLength; i++)
			{
				result = SetAction(i, this[i], result);

				if (this[i] == Actions.Empty)
				{
					result = SetAction(i, action, result);
					return new KuhnActions(result);
				}
			}
			
			throw new Exception("Can't add more actions");
		}

		public override bool Equals(object obj)
		{
			return (obj is KuhnActions a) && _data.Equals(a._data);
		}

		public override int GetHashCode()
		{
			return _data.GetHashCode();
		}
	}
}
