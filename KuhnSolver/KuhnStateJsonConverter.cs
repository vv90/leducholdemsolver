﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace KuhnSolver
{
	public class KuhnStateJsonConverter : JsonConverter<KuhnState>
	{
		public override void WriteJson(JsonWriter writer, KuhnState value, JsonSerializer serializer)
		{
			writer.WriteValue(value.ToString());
		}

		public override KuhnState ReadJson(JsonReader reader, Type objectType, KuhnState existingValue, bool hasExistingValue,
			JsonSerializer serializer)
		{
			return KuhnState.Parse((string) reader.Value);
		}
	}
}
