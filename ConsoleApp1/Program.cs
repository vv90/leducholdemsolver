﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using Dealer;
using Dealer.Interfaces;
using KuhnSolver;
using LeducHoldemSolver;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			var leducGame = new LeducGame();

			//			leducGame.RootNode.ExpandRecursive();
			//			ApplyAlwaysCheck(leducGame.RootNode);
			//			ApplyCheckCallPot(leducGame.RootNode);
			//			leducGame.Train(100000000);

			//			leducGame.Save("leduc_tree_alwaysCheck.json");
			//			leducGame.Save("leduc_tree_checkCallPot.json");

			//			leducGame.Load("leduc_tree_alwaysCheck.json");
			//			leducGame.Train(100000);
			//			leducGame.TrainPCS(100000);
			//			leducGame.TrainCfrPlus(100000);

			//			leducGame.Load("leduc_tree_1M.json");
			//			leducGame.Save("leduc_tree+10k.json");
			//			var exploitability = leducGame.RootNode.CalculateExploitability();

			//			Console.WriteLine($"Exploitability: {exploitability}");

			//			Play(10000);

//			TrainLeduc();
//			TrainKuhn(1000000);
			Play(1000);
			Console.WriteLine("Press any key to continue...");
			Console.ReadKey();
		}

		static void Play(int numIterations)
		{
			var player0 = new KuhnRandomPlayer();
//			var player1 = new KuhnHumanPlayer();
			var player1 = new KuhnRandomPlayer();
			var dealer = new Dealer<KuhnCard, KuhnPokerGame>();

			for (var i = 0; i < numIterations; i++)
			{
				
				dealer.Play(new KuhnState(), player0, player1);

//				Console.WriteLine(dealer.State.ToString());
//				while (!dealer.IsGameFinished)
//				{
//
//				}


			}

			Console.WriteLine($"player0: {player0.UtilityGain}, player1: {player1.UtilityGain}");
		}

//		static void TrainLeduc()
//		{
//			var rootNode = GameNode<Card>.TrainCfr(100000, new LeducState(), (progress, info) =>
//			{
//				Console.Clear();
//				Console.WriteLine($"{progress}%, {info}");
//			});
//		}

		static void TrainKuhn(int numIterations)
		{
			var rootNode = GameNode<KuhnCard, KuhnPokerGame>.TrainCfr(numIterations, new KuhnState(), (progress, info) =>
			{
				Console.Clear();
				Console.WriteLine($"{progress}%, {info}");
			});

			var exploitability = GameNode<KuhnCard, KuhnPokerGame>.CalculateExploitability(rootNode);
			Console.WriteLine($"Exploitability: {exploitability}");

			GameNode<KuhnCard, KuhnPokerGame>.Save(rootNode, "kuhn_tree_vanilla_100k.json");
		}

//		static void ApplyAlwaysCheck(LeducNode node)
//		{
//			if (!node.AggregateState.IsChanceNode)
//			{
//				foreach (var card in Card.EnumerateDeck())
//				{
//					if (!node.Values.ContainsKey(card))
//					{
//						node.Values.Add(card, new NodeValues(node.Children.Count));
//						var checkChildIndex = node.Children.FindIndex(ch =>
//							GetLastAction(ch.State).Action == Actions.Call && GetLastAction(ch.State).Amount <= 100);
//						var foldChildIndex = node.Children.FindIndex(ch => GetLastAction(ch.State).Action == Actions.Fold);
//
//						var strategyIndex = checkChildIndex >= 0 ? checkChildIndex : foldChildIndex;
//						if (node.Values[card].StrategySum.Length > 0)
//						{
//							node.Values[card].StrategySum[strategyIndex] = 1.0;
//						}
//					}
//				}
//			}
//
//			foreach (var child in node.Children)
//			{
//				ApplyAlwaysCheck(child);
//			}
//		}
//
//		static void ApplyCheckCallPot(LeducNode node)
//		{
//			if (!node.AggregateState.IsChanceNode)
//			{
//				foreach (var card in Card.EnumerateDeck())
//				{
//					if (!node.Values.ContainsKey(card))
//					{
//						node.Values.Add(card, new NodeValues(node.Children.Count));
//						var checkChildIndex = node.Children.FindIndex(ch =>
//							GetLastAction(ch.State).Action == Actions.Call && GetLastAction(ch.State).Amount <= 200);
//						var foldChildIndex = node.Children.FindIndex(ch => GetLastAction(ch.State).Action == Actions.Fold);
//
//						var strategyIndex = checkChildIndex >= 0 ? checkChildIndex : foldChildIndex;
//						if (node.Values[card].StrategySum.Length > 0)
//						{
//							node.Values[card].StrategySum[strategyIndex] = 1.0;
//						}
//					}
//				}
//			}
//
//			foreach (var child in node.Children)
//			{
//				ApplyCheckCallPot(child);
//			}
//		}
//
//		static PlayerAction GetLastAction(LeducState state)
//		{
//			var action = new PlayerAction();
//			for (var i = 0; i < LeducState.ActionsLength; i++)
//			{
//				if ((state.Board.IsEmpty() ? state.PreflopActions : state.FlopActions)[i].IsEmpty())
//				{
//					break;
//				}
//
//				action = (state.Board.IsEmpty() ? state.PreflopActions : state.FlopActions)[i];
//			}
//
//			return action;
//		}
	}
}
