﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CfrSolver.Enums;

namespace LeducHoldemSolver
{
	[StructLayout(LayoutKind.Explicit, Size = 8)]
	public struct PlayerAction : IEquatable<PlayerAction>
	{
		[FieldOffset(0)]
		private ulong _data;

		[FieldOffset(0)]
		public Actions Action;

		[FieldOffset(4)]
		public int Amount;

		public PlayerAction(Actions action, int amount)
		{
			_data = 0;
			Action = action;
			Amount = amount;
		}

		public static explicit operator PlayerAction(ulong value)
		{
			return new PlayerAction { _data = value };
		}

		public static explicit operator ulong(PlayerAction value)
		{
			return value._data;
		}

		[Pure]
		public bool IsEmpty()
		{
			return _data == 0;
		}

		[Pure]
		public override string ToString()
		{
			switch (Action)
			{
				case Actions.Fold:
					return $"f";
				case Actions.Call:
					return $"c{Amount}";
				case Actions.Raise:
					return $"r{Amount}";
				default:
					return "";
			}
		}

		public bool Equals(PlayerAction other)
		{
			return _data == other._data;
		}
	}
}
