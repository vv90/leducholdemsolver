﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LeducHoldemSolver
{
	public class NodeValues
	{
		[JsonProperty]
		public double[] Strategy { get; private set; }
		[JsonProperty]
		public double[] StrategySum { get; private set; }
		[JsonProperty]
		public double[] RegretSum { get; private set; }
		[JsonProperty]
		public Dictionary<Card, int> Utility { get; private set; }

		public NodeValues(int length)
		{
			Strategy = new double[length];
			StrategySum = new double[length];
			RegretSum = new double[length];

			if (length == 0)
			{
				Utility = new Dictionary<Card, int>();
			}
		}
	}

	[JsonObject(MemberSerialization.OptIn)]
	public class LeducNode
	{
//		public static readonly double[] BetSizings = new[] { 1.0, 2.0 };
//		public static readonly int StackSize = 10000;

		private static Random Rng = new Random();
		
		private AggregateState _aggregateState;

		public AggregateState AggregateState
		{
			get
			{
				if (_aggregateState == null)
				{
					SetAggregateState();
				}

				return _aggregateState;
			}
		}

		[JsonProperty]
		[JsonConverter(typeof(LeducStateJsonConverter))]
		public LeducState State { get; private set; }

		[JsonProperty]
		public Dictionary<Card, NodeValues> Values { get; private set; }

		[JsonProperty]
		public List<LeducNode> Children { get; private set; }

		public bool IsChanceNode { get; private set; }

		public static void Save(LeducNode node, string fileName)
		{
			File.WriteAllText(fileName, JsonConvert.SerializeObject(node));
		}

		public static LeducNode Load(string fileName)
		{
			return JsonConvert.DeserializeObject<LeducNode>(File.ReadAllText(fileName));
		}

		public LeducNode(LeducState state)
		{
			State = state;
		}

		public void Expand()
		{
			Children = new List<LeducNode>();
			foreach (var next in State.Expand())
			{
				Children.Add(new LeducNode(next as LeducState));
			}

			SetAggregateState();
			Values = new Dictionary<Card, NodeValues>();
			SetValues();
		}

		public void ExpandRecursive()
		{
			Expand();
			foreach (var child in Children)
			{
				child.ExpandRecursive();
			}
		}

		private int GetUtility(Card[] cards, int? player = null)
		{
			if (!Values.ContainsKey(cards[AggregateState.CurrentPlayer]))
			{
				Values.Add(cards[AggregateState.CurrentPlayer], new NodeValues(0));
			}

			if (!Values[cards[AggregateState.CurrentPlayer]].Utility.ContainsKey(cards[1 - AggregateState.CurrentPlayer]))
			{
				Values[cards[AggregateState.CurrentPlayer]].Utility.Add(
					cards[1 - AggregateState.CurrentPlayer],
					State.GetTerminalUtility(cards).Value
				);
			}
			
			return (player == null || player.Value == AggregateState.CurrentPlayer ? 1 : -1) * 
			       Values[cards[AggregateState.CurrentPlayer]].Utility[cards[1 - AggregateState.CurrentPlayer]];
		}

		public double Cfr(Card[] cards, double p0, double p1)
		{
			if (Children == null)
			{
				Expand();
			}

			if (Children.Count == 0)
			{
				return GetUtility(cards);
			}

			var util = new double[Children.Count];
			var nodeUtil = 0.0;

			if (AggregateState.IsChanceNode)
			{
				nodeUtil = Children.Find(ch => ch.State.Board.Equals(cards[2])).Cfr(cards, p0, p1);

			}
			else
			{
//				if (!Values.ContainsKey(cards[AggregateState.CurrentPlayer]))
//				{
//					Values.Add(cards[AggregateState.CurrentPlayer], new NodeValues(Children.Count));
//				}

				var value = Values[cards[AggregateState.CurrentPlayer]];
				for (var i = 0; i < Children.Count; i++)
				{
//					if (value.Strategy[i] == 0)
//					{
//						continue;
//					}

					util[i] = AggregateState.CurrentPlayer == 0
						? -Children[i].Cfr(cards, p0 * value.Strategy[i], p1)
						: -Children[i].Cfr(cards, p0, p1 * value.Strategy[i]);

					nodeUtil += value.Strategy[i] * util[i];
				}

				for (var i = 0; i < Children.Count; i++)
				{
					var regret = util[i] - nodeUtil;
					value.RegretSum[i] += (AggregateState.CurrentPlayer == 0 ? p1 : p0) * regret;
				}

				UpdateStrategy(cards[AggregateState.CurrentPlayer], AggregateState.CurrentPlayer == 0 ? p0 : p1);
			}

//			LastUtil = nodeUtil;
			return nodeUtil;
		}

		public double[] GetPcsTerminalUtility(int player, double[] reachProbabilities, Card[] deck)
		{
			var cards = new Card[2];
			var utility = new double[deck.Length];

			for (var i = 0; i < deck.Length; i++)
			{
				if (deck[i].Equals(State.Board))
				{
					continue;
				}


				for (var j = 0; j < deck.Length; j++)
				{
					if (i == j || deck[j].Equals(State.Board))
					{
						continue;
					}


					cards[player] = deck[i];
					cards[1 - player] = deck[j];

					utility[i] += reachProbabilities[j] * GetUtility(cards, player) / (deck.Length - 2);
				}

				utility[i] /= (deck.Length - 1);
			}

			return utility;
		}

		public double[] CfrPlus(int player, int weight, double[] reachProbabilities, Card[] deck)
		{
			if (Children == null)
			{
				Expand();
			}

			if (Children.Count == 0)
			{
				return GetPcsTerminalUtility(player, reachProbabilities, deck);
			}

			var util = new double[deck.Length];

			if (AggregateState.IsChanceNode)
			{
				foreach (var child in Children)
				{
					util.ElementwizeApply(child.CfrPlus(player, weight, reachProbabilities, deck), (utl, u) => utl + u / deck.Length);
				}

				return util;
			}

			var strategy = new double[Children.Count, deck.Length];
			var childUtilities = new double[Children.Count, deck.Length];
			var nodeUtility = new double[deck.Length];
			var cardIndexes = deck.Select((c, i) => c.Equals(State.Board) ? -1 : i).Where(i => i >= 0).ToArray();

			// regret matching
			foreach (var i in cardIndexes)
			{
				var normalizingSum = 0.0;
				for (var a = 0; a < Children.Count; a++)
				{
					strategy[a, i] = Math.Max(Values[deck[i]].RegretSum[a], 0);
					normalizingSum += strategy[a, i];
				}

				for (var a = 0; a < Children.Count; a++)
				{
					strategy[a, i] = normalizingSum > 0
						? strategy[a, i] / normalizingSum
						: 1.0 / Children.Count;
				}
			}

			if (AggregateState.CurrentPlayer == player)
			{
				for (var a = 0; a < Children.Count; a++)
				{
					var childUtil = Children[a].CfrPlus(player, weight, reachProbabilities, deck);
					for (var i = 0; i < deck.Length; i++)
					{
						childUtilities[a, i] += childUtil[i];
						nodeUtility[i] += childUtil[i] * strategy[a, i];
					}
				}

				foreach (var i in cardIndexes)
				{
					for (var a = 0; a < Children.Count; a++)
					{
						Values[deck[i]].RegretSum[a] = Math.Max(Values[deck[i]].RegretSum[a] + childUtilities[a, i] - nodeUtility[i], 0.0);
					}
				}
			}
			else
			{
				for (var a = 0; a < Children.Count; a++)
				{
					var updatedReachProbabilities = reachProbabilities.ToArray();
					for (var i = 0; i < deck.Length; i++)
					{
						updatedReachProbabilities[i] = strategy[a, i] * reachProbabilities[i];
					}

					var childUtil = Children[a].CfrPlus(player, weight, updatedReachProbabilities, deck);

					for (var i = 0; i < deck.Length; i++)
					{
						nodeUtility[i] += childUtil[i];
					}
				}

				foreach (var i in cardIndexes)
				{
					for (var a = 0; a < Children.Count; a++)
					{
						Values[deck[i]].StrategySum[a] += reachProbabilities[i] * strategy[a, i];
					}
				}
			}

			return nodeUtility;
		}

		public double[] PcsCfr(int player, double[,] reachProbabilities, Card[] deck, Card board)
		{
			if (Children == null)
			{
				Expand();
			}

			if (Children.Count == 0)
			{
				var opponentReachProbabilities = new double[deck.Length];
				for (var i = 0; i < deck.Length; i++)
				{
					opponentReachProbabilities[i] = reachProbabilities[1 - player, i];
				}
				return GetPcsTerminalUtility(player, opponentReachProbabilities, deck);
			}

			if (AggregateState.IsChanceNode)
			{
				return Children.Find(ch => ch.State.Board.Equals(board)).PcsCfr(player, reachProbabilities, deck, board);
			}


			var strategy = new double[Children.Count, deck.Length];
			var nodeUtility = new double[deck.Length];
			var childUtilities = new double[Children.Count, deck.Length];
			var cardIndexes = deck.Select((c, i) => c.Equals(State.Board) ? -1 : i).Where(i => i >= 0).ToArray();

//			foreach (var i in cardIndexes)
//			{
//				if (!Values.ContainsKey(deck[i]))
//				{
//					Values.Add(deck[i], new NodeValues(Children.Count));
//				}
//			}

			// regret matching
			//foreach (var playerCard in Values.Keys)
			foreach (var i in cardIndexes)
			{
				var normalizingSum = 0.0;
				for (var a = 0; a < Children.Count; a++)
				{
					strategy[a, i] = Math.Max(Values[deck[i]].RegretSum[a], 0);
					normalizingSum += strategy[a, i];
				}

				for (var a = 0; a < Children.Count; a++)
				{
					strategy[a, i] = normalizingSum > 0 
						? strategy[a, i] / normalizingSum 
						: 1.0 / Children.Count;
				}
			}

			for (var a = 0; a < Children.Count; a++)
			{
				var updatedReachProbabilities = new double[2, deck.Length];
				if (AggregateState.CurrentPlayer == player)
				{
					foreach (var i in cardIndexes)
					{
						updatedReachProbabilities[player, i] = reachProbabilities[player, i] * strategy[a, i];
						updatedReachProbabilities[1 - player, i] = reachProbabilities[1 - player, i];
					}

					var childUtility = Children[a].PcsCfr(player, updatedReachProbabilities, deck, board);
					foreach (var i in cardIndexes)
					{
						childUtilities[a, i] = childUtility[i];
						nodeUtility[i] += strategy[a, i] * childUtility[i];
					}
				}
				else
				{
					foreach (var i in cardIndexes)
					{
						updatedReachProbabilities[player, i] = reachProbabilities[player, i];
						updatedReachProbabilities[1 - player, i] = reachProbabilities[1 - player, i] * strategy[a, i];
					}

					var childUtility = Children[a].PcsCfr(player, updatedReachProbabilities, deck, board);

					foreach (var i in cardIndexes)
					{
						nodeUtility[i] += childUtility[i];
					}
				}
			}

			if (AggregateState.CurrentPlayer == player)
			{
				foreach (var i in cardIndexes)
				{
					for (var a = 0; a < Children.Count; a++)
					{
						Values[deck[i]].RegretSum[a] += childUtilities[a, i] - nodeUtility[i];
						Values[deck[i]].StrategySum[a] += reachProbabilities[player, i] * strategy[a, i];
					}
				}
			}

			return nodeUtility;
		}

		public Dictionary<PlayerAction, double> LookupStrategy(LeducState state, Card card)
		{
			if (State.Equals(state))
			{
				var normalizingSum = Values[card].StrategySum.Sum();
				return Children.Select((c, i) => new {action = c.State.GetLastAction(), value = Values[card].StrategySum[i] / normalizingSum})
					.ToDictionary(item => item.action, item => item.value);
			}
			else
			{
				var child = Children.Find(c => c.State.PartialMatch(state));
				if (child == null)
				{
					throw new Exception("Could not resolve strategy");
				}

				return child.LookupStrategy(state, card);
			}
			
		}

		private void SetValues()
		{
			foreach (var card in Card.EnumerateLeducDeck())
			{
				if (!card.Equals(State.Board) && !Values.ContainsKey(card))
				{
					Values.Add(card, new NodeValues(Children.Count));
				}
			}
		}

		public double CalculateExploitability()
		{
			var deck = Card.EnumerateLeducDeck().ToArray();
			var util = 0.0;

			for (var player = 0; player <= 1; player++)
			{
				foreach (var hand in deck)
				{
					var cardProbabilities = deck.Select(c => (c.Equals(hand) ? 0.0 : 1.0) / (deck.Length - 1)).ToArray();
					util += BestResponse(player, hand, cardProbabilities, deck) / deck.Length;
				}
			}

			return util / 2;
		}

		public double BestResponse(int player, Card hand, double[] cardProbabilities, Card[] deck)
		{
			if (_aggregateState == null)
			{
				SetAggregateState();
			}

			if (Children.Count == 0)
			{
				var probabilities = cardProbabilities.Select((p, i) => deck[i].Equals(State.Board) || deck[i].Equals(hand) ? 0.0 : p).ToArray();
				var cardDistribution = Normalize(probabilities);
				var nodeUtil = 0.0;

				for (var i = 0; i < deck.Length; i++)
				{
					if (deck[i].Equals(hand) || deck[i].Equals(State.Board))
					{
						continue;
					}

					var cards = new Card[2];
					cards[player] = hand;
					cards[1 - player] = deck[i];

					nodeUtil += cardDistribution[i] * GetUtility(cards) * (_aggregateState.CurrentPlayer == player ? 1 : -1);
				}

				return nodeUtil;
			}
			else if (_aggregateState.IsChanceNode)
			{
				return Children.Where(ch => !ch.State.Board.Equals(hand)).Sum(ch => 
					ch.BestResponse(
						player, 
						hand, 
						cardProbabilities.Select((p, i) => deck[i].Equals(ch.State.Board) ? 0 : p).ToArray(), 
						deck
					) / (Children.Count - 1));
			}


			var util = new double[Children.Count];
			var weights = new double[Children.Count];
			

			for (var i = 0; i < Children.Count; i++)
			{
				var childProbabilities = cardProbabilities.ToArray();
				if (_aggregateState.CurrentPlayer != player)
				{
					weights[i] = ComputeWeight(childProbabilities, i, deck);
				}
				util[i] = Children[i].BestResponse(player, hand, childProbabilities, deck);
			}



			if (_aggregateState.CurrentPlayer != player)
			{
				var actionDistribution = Normalize(weights);

				return util.Select((u, i) => u * actionDistribution[i]).Sum();
			}

			return util.Max();
		}

		private double[] Normalize(double[] p)
		{
			var sum = p.Sum();
			return p.Select(i => sum > 0 ? i / sum : 0).ToArray();
		}

		private double ComputeWeight(double[] p, int strategyIndex, Card[] deck)
		{

			for (var i = 0; i < deck.Length; i++)
			{
				if (!Values.ContainsKey(deck[i]))
				{
					continue;
				}

				var normalizingSum = Values[deck[i]].StrategySum.Sum();
				var strategy = Values[deck[i]].StrategySum[strategyIndex] / normalizingSum;

				p[i] = p[i] * strategy;
			}

			return p.Sum();
		}

		private void SetAggregateState()
		{
			_aggregateState = State.GetAggregateState();
			IsChanceNode = State.GetAggregateState().IsChanceNode;
		}

		private void UpdateStrategy(Card hand, double realizationWeight)
		{
			double normalizingSum = 0;
			
			for (var i = 0; i < Children.Count; i++)
			{
				Values[hand].Strategy[i] = Values[hand].RegretSum[i] > 0 ? Values[hand].RegretSum[i] : 0;
				normalizingSum += Values[hand].Strategy[i];
			}

			for (var i = 0; i < Children.Count; i++)
			{
				Values[hand].Strategy[i] = normalizingSum > 0 ? Values[hand].Strategy[i] / normalizingSum : 1.0 / Children.Count;
				Values[hand].StrategySum[i] += realizationWeight * Values[hand].Strategy[i];
			}
		}
	}

}
