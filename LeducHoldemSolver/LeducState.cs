﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Enums;
using Newtonsoft.Json;

namespace LeducHoldemSolver
{
	
	public class LeducState : IEquatable<LeducState>
	{
		#region static members
		public const int ActionsLength = 4;
		public const int StackSize = 10000;
		public static readonly double[] BetSizings = new double[] {1.0, 2.0};

		private static readonly Regex StateRegex = new Regex(@"([crf])(\d+)?|([AKQ][hs])");
		private static readonly Dictionary<string, Actions> ActionsDictionary = new Dictionary<string, Actions>
		{
			{ "c", Actions.Call },
			{ "r", Actions.Raise },
			{ "f", Actions.Fold }
		};

		public static LeducState Parse(string value)
		{
			Parse(value, out var preflopActions, out var board, out var flopActions);

			return new LeducState(preflopActions, board, flopActions);
		}

		private static void Parse(string value, out LeducActions preflopActions, out Card board, out LeducActions flopActions)
		{
			var matches = StateRegex.Matches(value);
			var preflop = new List<PlayerAction>();
			var flop = new List<PlayerAction>();
//			preflopActions = new LeducActions();
//			flopActions = new LeducActions();
			board = default(Card);

			foreach (var match in matches)
			{
				var m = match as Match;
				if (m.Groups[1].Success)
				{
					(board.IsEmpty() ? preflop : flop).Add(ParseAction(m.Groups[1].Value, m.Groups[2].Value));
				}
				else if (m.Groups[3].Success)
				{
					Card.TryParse(m.Groups[3].Value, out board);
				}
			}

			preflopActions = new LeducActions(preflop);
			flopActions = new LeducActions(flop);
		}

		private static PlayerAction ParseAction(string actionStr, string amountStr)
		{
			if (!ActionsDictionary.ContainsKey(actionStr))
			{
				throw new ArgumentException("Invalid action string", nameof(actionStr));
			}

			int amount;
			bool amountParseSuccess;
			if (string.IsNullOrEmpty(amountStr))
			{
				amountParseSuccess = true;
				amount = 0;
			}
			else
			{
				amountParseSuccess = int.TryParse(amountStr, out amount);
			}

			if (!amountParseSuccess)
			{
				throw new ArgumentException("Invalid amount string", nameof(amountStr));
			}

			return new PlayerAction(ActionsDictionary[actionStr], amount);
		}
		#endregion

		public readonly Card Board;

		public readonly LeducActions PreflopActions;
		public readonly LeducActions FlopActions;

		private AggregateState _aggregateState;



		public LeducState()
		{
		}

		public LeducState(string value)
		{
			Parse(value, out var preflopActions, out var board, out var flopActions);
			PreflopActions = preflopActions;
			FlopActions = flopActions;
			Board = board;
		}

		public LeducState(LeducActions preflopActions, Card board = new Card(), LeducActions flopActions = new LeducActions())
		{
			Board = board;
			PreflopActions = preflopActions;
			FlopActions = flopActions;
		}

		public LeducState(IEnumerable<PlayerAction> preflopActions, Card board = new Card(), IEnumerable<PlayerAction> flopActions = null) 
			: this(new LeducActions(preflopActions), board, flopActions == null ? new LeducActions() : new LeducActions(flopActions))
		{
		}

		
		private static int GetUtility(IReadOnlyList<int> bets, int commonPot, int winner)
		{
			return commonPot / 2 + bets[1 - winner];
		}
		
		
		public int? GetTerminalUtility(Card[] cards)
		{
			var aggregateState = GetAggregateState();

			if (!aggregateState.IsTerminal) return null;

			if (aggregateState.PlayerPlaying[0] != aggregateState.PlayerPlaying[1])
			{
				var winner = aggregateState.PlayerPlaying[0] ? 0 : 1;
				return (aggregateState.CurrentPlayer == winner ? 1 : -1) * 
				       GetUtility(aggregateState.Bets, aggregateState.CommonPot, winner);
			}
			else
			{
				if (cards.Length < 2 || cards[0].IsEmpty() || cards[1].IsEmpty())
				{
					throw new Exception("Opponent hand is empty");
				}
				var winner = GetWinner(aggregateState.CurrentPlayer, cards);

				if (winner.HasValue)
				{
					return (aggregateState.CurrentPlayer == winner.Value ? 1 : -1) *
					       GetUtility(aggregateState.Bets, aggregateState.CommonPot, winner.Value);
				}
				else
				{
					return 0;
				}
				
			}

		}

		public int GetUtility(Card[] privateStates)
		{
			return GetTerminalUtility(privateStates).Value;
		}

		public int GetUtility(Card[] privateStates, int player)
		{
			return GetAggregateState().CurrentPlayer == player ? GetUtility(privateStates) : -GetUtility(privateStates);
		}

		public bool IsTerminal()
		{
			return GetAggregateState().IsTerminal;
		}

		public int CurrentPlayer()
		{
			return GetAggregateState().CurrentPlayer;
		}

		public bool IsChanceNode()
		{
			return GetAggregateState().IsChanceNode;
		}

		public IEnumerable<Card> GetPossiblePrivateStates()
		{
			return Card.EnumerateLeducDeck().Where(c => !c.Equals(Board));
		}

		public bool IsValidCombination(Card[] privateCards)
		{
			for (var i = 0; i < privateCards.Length; i++)
			{
				if (privateCards[i].Equals(Board))
				{
					return false;
				}
				for (var j = i + 1; j < privateCards.Length; j++)
				{
					if (privateCards[i].Equals(privateCards[j]))
					{
						return false;
					}
				}
			}
			return true;
		}

		public AggregateState GetAggregateState()
		{
			if (_aggregateState == null)
			{
				_aggregateState = ComputeAggregateState(StackSize);
			}

			return _aggregateState;
		}

		public PlayerAction GetLastAction()
		{
			return Board.IsEmpty() ? PreflopActions.Last() : FlopActions.Last();
		}

		public bool PartialMatch(LeducState other)
		{
			for (var i = 0; i < ActionsLength; i++)
			{
				if (PreflopActions[i].IsEmpty() && other.PreflopActions[i].IsEmpty())
				{
					break;
				}
				
				if (PreflopActions[i].IsEmpty() || other.PreflopActions[i].IsEmpty())
				{
					return true;
				}

				if (!PreflopActions[i].Equals(other.PreflopActions[i]))
				{
					return false;
				}
			}

			if (Board.IsEmpty() || other.Board.IsEmpty())
			{
				return true;
			}

			if (!Board.Equals(other.Board))
			{
				return false;
			}

			for (var i = 0; i < ActionsLength; i++)
			{
				if (FlopActions[i].IsEmpty() || other.FlopActions[i].IsEmpty())
				{
					return true;
				}

				if (!FlopActions[i].Equals(other.FlopActions[i]))
				{
					return false;
				}
			}

			return false;
		}

		private AggregateState ComputeAggregateState(int stackSize)
		{
			var aggregateState = new AggregateState();

			for (var betRound = BetRounds.Preflop; betRound <= BetRounds.Flop; betRound++)
			{
				if (betRound == BetRounds.Flop && !Board.IsEmpty())
				{
					aggregateState.FinishBetRound();
				}

				if (betRound == BetRounds.Flop && Board.IsEmpty())
				{
					break;
				}

				for (var i = 0; i < ActionsLength; i++)
				{
					aggregateState.CurrentPlayer = (i % 2);
					if (aggregateState.IsTerminal)
					{
						return aggregateState;
					}

					var action = betRound == BetRounds.Preflop ? PreflopActions[i] : FlopActions[i];
					if (action.IsEmpty())
					{
						break;
					}

					aggregateState.PlayerActed[aggregateState.CurrentPlayer] = true;
					aggregateState.NumActions += 1;

					if (action.Action == Actions.Fold)
					{
						//					terminalUtility = GetUtility(bets, commonPot, 1 - (i % 2));
						aggregateState.IsTerminal = true;
						aggregateState.PlayerPlaying[aggregateState.CurrentPlayer] = false;
					}
					else
					{
						aggregateState.Bets[aggregateState.CurrentPlayer] = action.Amount;
					}
				}

			}
			
			if (aggregateState.CommonPot / 2 + aggregateState.Bets[0] >= stackSize && 
			    aggregateState.CommonPot / 2 + aggregateState.Bets[1] >= stackSize)
			{
				aggregateState.IsTerminal = !Board.IsEmpty();
			}

			if (aggregateState.PlayerActed.All(acted => acted) && aggregateState.Bets[0] == aggregateState.Bets[1])
			{
				aggregateState.IsTerminal = !Board.IsEmpty();
			}

			aggregateState.IsChanceNode = Board.IsEmpty() && aggregateState.BetRoundFinished();
		
			return aggregateState;
		}

		
		
		public IEnumerable<LeducState> Expand()
		{
			var aggregateState = GetAggregateState();
			
			if (aggregateState.IsTerminal)
			{
				yield break;
			}


			if (Board.IsEmpty() && aggregateState.BetRoundFinished())
			{
//					terminalUtility = null;
				foreach (var card in Card.EnumerateLeducDeck())
				{
					yield return new LeducState(PreflopActions, card);
				}
			}
			else
			{
				yield return Board.IsEmpty() 
					? new LeducState(PreflopActions.Add(new PlayerAction(Actions.Fold, 0)))
					: new LeducState(PreflopActions, Board, FlopActions.Add(new PlayerAction(Actions.Fold, 0)));


				yield return Board.IsEmpty() 
					? new LeducState(PreflopActions.Add(new PlayerAction(Actions.Call, aggregateState.Bets.Max())))
					: new LeducState(PreflopActions, Board, FlopActions.Add(new PlayerAction(Actions.Call, aggregateState.Bets.Max())));


				if (aggregateState.NumActions < ActionsLength - 1)
				{
					foreach (var sizing in BetSizings)
					{
						var amount = (int) ((aggregateState.CommonPot + aggregateState.Bets[0] + aggregateState.Bets[1]) * sizing) +
						             aggregateState.Bets[aggregateState.CurrentPlayer];

						if (aggregateState.CommonPot / 2 + amount > StackSize)
						{
							amount = StackSize;
						}

						if (amount > aggregateState.Bets[0] && amount > aggregateState.Bets[1])
						{
							yield return Board.IsEmpty()
								? new LeducState(PreflopActions.Add(new PlayerAction(Actions.Raise, amount)))
								: new LeducState(PreflopActions, Board, FlopActions.Add(new PlayerAction(Actions.Raise, amount)));
						}

						if (amount == StackSize)
						{
							yield break;
						}
					}
				}
			}
		}

		public string Serialize()
		{
			return ToString();
		}

		public LeducState Deserialize(string value)
		{
			return Parse(value);
		}

		public IEnumerable<LeducState> ExpandChance()
		{
			return null;
		}

		public override string ToString()
		{
			var sb = new StringBuilder();
			
			for (var i = 0; i < ActionsLength; i++)
			{
				sb.Append(PreflopActions[i]);
			}

			if (!Board.IsEmpty())
			{
				sb.Append(Board);

				for (var i = 0; i < ActionsLength; i++)
				{
					sb.Append(FlopActions[i]);
				}
			}

			return sb.ToString();
		}
		
		
		private int? GetWinner(int currentPlayer, Card[] cards)
		{
			if (cards[currentPlayer].Rank == Board.Rank)
			{
				return currentPlayer;
			}
			else if (cards[1 - currentPlayer].Rank == Board.Rank)
			{
				return 1 - currentPlayer;
			}
			else if (cards[currentPlayer].Rank == cards[1 - currentPlayer].Rank)
			{
				return null;
			}
			else if (cards[currentPlayer].Rank > cards[1 - currentPlayer].Rank)
			{
				return currentPlayer;
			}
			else if (cards[currentPlayer].Rank < cards[1 - currentPlayer].Rank)
			{
				return 1 - currentPlayer;
			}

			throw new Exception("Can't get the winner");
		}
//
//		public bool Equals(LeducState other)
//		{
//			return (other is LeducState state) && Equals(state);
//		}

		public bool Equals(LeducState other)
		{
			return Board.Equals(other.Board) &&
			       PreflopActions.Equals(other.PreflopActions) &&
			       FlopActions.Equals(other.FlopActions);
		}

		public override bool Equals(object obj)
		{
			return (obj is LeducState state) && Equals(state);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + Board.GetHashCode();
				hash = hash * 23 + PreflopActions.GetHashCode();
				hash = hash * 23 + FlopActions.GetHashCode();
				return hash;
			}
		}
	}
}
