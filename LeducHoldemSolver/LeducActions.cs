﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver.Enums;

namespace LeducHoldemSolver
{
	public unsafe struct LeducActions : IEquatable<LeducActions>
	{
		private fixed ulong _actions[LeducState.ActionsLength];

		public LeducActions(IEnumerable<PlayerAction> actions)
		{
			var index = 0;

			fixed (ulong* actPtr = _actions)
			{
				foreach (var act in actions)
				{
					if (index >= LeducState.ActionsLength)
					{
						break;
					}

					// this[index] = act;
					actPtr[index] = (ulong)act;
					index += 1;
				}
			}
		}

		private LeducActions(LeducActions existingActions, PlayerAction newAction)
		{
			var index = 0;
			while (index < LeducState.ActionsLength && !existingActions[index].IsEmpty())
			{
				this[index] = existingActions[index];
				index += 1;
			}

			if (index < LeducState.ActionsLength)
			{
				this[index] = newAction;
			}
		}

		private LeducActions(LeducActions existingActions, Actions action, int commonPot, double? sizing = null)
		{
			var index = 0;
			var bets = new int[2];
			while (index < LeducState.ActionsLength && !existingActions[index].IsEmpty())
			{
				this[index] = existingActions[index];
				bets[index % 2] = existingActions[index].Amount;
				index += 1;
			}

			if (index < LeducState.ActionsLength)
			{
				switch (action)
				{
					case Actions.Fold:
						this[index] = new PlayerAction(action, 0);
						break;
					case Actions.Call:
						this[index] = new PlayerAction(action, bets.Max());
						break;
					case Actions.Raise:
						if (!sizing.HasValue)
						{
							throw new ArgumentException("Missing bet sizing");
						}

						if (commonPot == 0 && bets[0] == 0)
						{
							bets[0] = 100;
						}

						if (commonPot == 0 && bets[1] == 0)
						{
							bets[1] = 100;
						}

						this[index] = new PlayerAction(action, (int)((bets.Sum() + commonPot) * sizing.Value) + bets[index % 2]);
						break;
				}

			}
		}

		public PlayerAction this[int index]
		{
			get
			{
				if (index >= LeducState.ActionsLength || index < 0)
				{
					throw new IndexOutOfRangeException();
				}

				PlayerAction action;
				fixed (ulong* actions = _actions)
				{
					action = (PlayerAction)actions[index];
				}

				return action;
			}
			private set
			{
				if (index >= LeducState.ActionsLength || index < 0)
				{
					throw new IndexOutOfRangeException();
				}

				fixed (ulong* actions = _actions)
				{
					actions[index] = (ulong)value;
				}
			}
		}

		[Pure]
		public PlayerAction Last()
		{
			var action = this[0];
			for (var i = 0; i < LeducState.ActionsLength; i++)
			{
				if (this[i].IsEmpty())
				{
					return action;
				}

				action = this[i];
			}

			return action;
		}

		[Pure]
		public bool IsEmpty()
		{
			fixed (ulong* actions = _actions)
			{
				for (var i = 0; i < LeducState.ActionsLength; i++)
				{
					if (actions[i] != 0)
					{
						return false;
					}
				}
			}

			return true;
		}

		[Pure]
		public LeducActions Add(PlayerAction action)
		{
			return new LeducActions(this, action);
		}

		[Pure]
		public LeducActions AddFold(int commonPot)
		{
			return new LeducActions(this, Actions.Fold, commonPot);
		}

		[Pure]
		public LeducActions AddCall(int commonPot)
		{
			return new LeducActions(this, Actions.Call, commonPot);
		}

		[Pure]
		public LeducActions AddRaise(int commonPot, double sizing)
		{
			return new LeducActions(this, Actions.Raise, commonPot, sizing);
		}

		[Pure]
		public bool Equals(LeducActions other)
		{
			fixed (ulong* actions = _actions)
			{
				for (var i = 0; i < LeducState.ActionsLength; i++)
				{
					if (actions[i] != other._actions[i])
					{
						return false;
					}
				}
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			return (obj is LeducActions act) && Equals(act);
		}

		public override int GetHashCode()
		{
			fixed (ulong* data = _actions)
			{
				int hash = 17;

				for (var i = 0; i < LeducState.ActionsLength; i++)
				{
					hash = hash * 23 + (int) data[i];
					hash = hash * 23 + (int) (data[i] >> 32);
				}

				return hash;
			}
		}
	}
}
