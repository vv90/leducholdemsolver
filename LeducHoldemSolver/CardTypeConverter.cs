﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeducHoldemSolver
{
	public class CardTypeConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (!(value is string cardStr))
			{
				return base.ConvertFrom(context, culture, value);
			}

			var success = Card.TryParse(cardStr, out var card);

			if (!success)
			{
				throw new Exception($"Unable to parse card {cardStr}");
			}

			return card;
		}
		// Overrides the ConvertTo method of TypeConverter.
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			return destinationType == typeof(string) 
				? ((Card) value).ToString() 
				: base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
