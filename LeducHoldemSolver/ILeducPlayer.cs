﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeducHoldemSolver
{
	public class CardDealtEventArgs : EventArgs
	{
		public Card Card { get; set; }
	}

	public class PlayerActingEventArgs : EventArgs
	{
		public PlayerAction Action { get; set;}
		public PlayerActingEventArgs(PlayerAction action)
		{
			Action = action;
		}
	}

	public interface ILeducPlayer
	{
		event EventHandler<PlayerActingEventArgs> PlayerActingEvent;
		int UtilityGain { get; }
		void SetCard(Card card);
		void SetState(LeducState state, IEnumerable<PlayerAction> possibleActions);
		void SetUtility(int amount);
	}
}
