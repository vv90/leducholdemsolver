﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CfrSolver;
using CfrSolver.Enums;
using CfrSolver.Interfaces;
using Newtonsoft.Json;

namespace LeducHoldemSolver
{
	[TypeConverter(typeof(CardTypeConverter))]
	public struct Card : IPrivateState, IEquatable<Card>
	{
		public static Dictionary<Ranks, char> RankChars = new Dictionary<Ranks, char>
		{
			{ Ranks.Ace, 'A' },
			{ Ranks.King, 'K' },
			{ Ranks.Queen, 'Q' }
		};

		public static Dictionary<Suits, char> SuitChars = new Dictionary<Suits, char>
		{
			{ Suits.Hearts, 'h' },
			{ Suits.Spades, 's' }
		};

		public static IEnumerable<Card> EnumerateLeducDeck()
		{
			foreach (var rank in RankChars.Keys)
			{
				foreach (var suit in SuitChars.Keys)
				{
					yield return new Card(rank, suit);
				}
			}
		}

		public static bool TryParse(string cardStr, out Card card)
		{
			if (cardStr.Length < 2 || !RankChars.ContainsValue(cardStr[0]) || !SuitChars.ContainsValue(cardStr[1]))
			{
				card = new Card();
				return false;
			}

			card = new Card(RankChars.First(r => r.Value == cardStr[0]).Key, SuitChars.First(s => s.Value == cardStr[1]).Key);
			return true;
		}

		public Card(Ranks rank, Suits suit)
		{
			Rank = rank;
			Suit = suit;
		}

		public readonly Ranks Rank;
		public readonly Suits Suit;

		[Pure]
		public bool IsEmpty()
		{
			return Rank == Ranks.Empty || Suit == Suits.Empty;
		}

//		public bool IsValidCombination(IPrivateState other)
//		{
//			return (other is Card c) && !Equals(c);
//		}

		public override string ToString()
		{
			return IsEmpty() ? "" : $"{RankChars[Rank]}{SuitChars[Suit]}";
		}

		public bool Equals(IPrivateState other)
		{
			return (other is Card c) && Equals(c);
		}

		public bool Equals(Card other)
		{
			return Rank == other.Rank && Suit == other.Suit;
		}

		public override bool Equals(object obj)
		{
			return (obj is Card c) && Equals(c);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + (int) Rank;
				hash = hash * 23 + (int) Suit;
				return hash;
			}
		}
	}
}
