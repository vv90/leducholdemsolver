﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeducHoldemSolver
{
	public static class VectorUtils
	{
		public static double[] VectorAdd(this double[] vector, double[] other)
		{
			if (vector.Length != other.Length)
			{
				throw new ArgumentException("Can not add vectors with different lengths");
			}

			return vector.Zip(other, ((v1, v2) => v1 + v2)).ToArray();
		}

		public static void ElementwizeApply<T>(this T[] target, T[] other, Func<T, T, T> operation)
		{
			if (target.Length != other.Length)
			{
				throw new ArgumentException("Lengths do not match");
			}

			for (var i = 0; i < target.Length; i++)
			{
				target[i] = operation(target[i], other[i]);
			}
		}

		public static void ElementwizeApply<T>(this T[,] target, int firstIndex, T[] other, Func<T, T, T> operation)
		{
			if (target.GetLength(1) != other.Length)
			{
				throw new ArgumentException("Lengths do not match");
			}

			for (var i = 0; i < other.Length; i++)
			{
				target[firstIndex, i] = operation(target[firstIndex, i], other[i]);
			}
		}

		public static void Normalize(this double[] target)
		{
			var normalizingSum = target.Sum();
			for (var i = 0; i < target.Length; i++)
			{
				target[i] /= normalizingSum;
			}
		}
	}
}
