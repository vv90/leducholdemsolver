﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeducHoldemSolver
{
	public static class CfrUtils
	{
		public static void UpdateStrategy(double[] strategy, double[] strategySum, double[] regretSum, double realizationWeight)
		{
			var normalizingSum = 0.0;

			for (var a = 0; a < regretSum.Length; a++)
			{
				strategy[a] = Math.Max(regretSum[a], 0);
				normalizingSum += strategy[a];
			}

			for (var a = 0; a < regretSum.Length; a++)
			{
				strategy[a] = normalizingSum > 0 ? strategy[a] / normalizingSum : 1.0 / regretSum.Length;
				strategySum[a] += realizationWeight * strategy[a];
			}
		}

		public static void UpdateRegrets(double[] regretSum, double nodeUtil, double[] childUtils, double reachProbability)
		{
			for (var a = 0; a < regretSum.Length; a++)
			{
				regretSum[a] += (childUtils[a] - nodeUtil) * reachProbability;
			}
		}
	}
}
