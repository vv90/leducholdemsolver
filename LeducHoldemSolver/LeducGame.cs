﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CfrSolver.Enums;
using Newtonsoft.Json;

namespace LeducHoldemSolver
{
	

	

	

	public enum BetRounds
	{
		Preflop,
		Flop
	}
	
	public class ExecutionState
	{
		public double result;
		public int count;
	}

	public class LeducGame
	{
		private static readonly Random Rng = new Random();
		private static readonly Card[] Deck = new[]
		{
			new Card(Ranks.Queen, Suits.Hearts),
			new Card(Ranks.King, Suits.Hearts),
			new Card(Ranks.Ace, Suits.Hearts),
			new Card(Ranks.Queen, Suits.Spades),
			new Card(Ranks.King, Suits.Spades),
			new Card(Ranks.Ace, Suits.Spades)
		};

		public LeducNode RootNode { get; private set; } = new LeducNode(new LeducState());

		public LeducGame()
		{
		}

		public void Train(int numIterations)
		{
			var util = 0.0;
			var lastUpdate = 0;
			var lastExploitabilityUpdate = 0;
			var progress = 0.0;
			var time = DateTime.Now;
			var totalCount = 0;
			var exploitability = 0.0;
			var screenUpdateRange = numIterations / 1000;
			var exploitabilityUpdateRange = numIterations / 100;

			var cards = new Card[3];
			for (var i = 0; i < numIterations; i++)
			{
				ShuffleCards();
				cards[0] = Deck[0];
				cards[1] = Deck[1];
				cards[2] = Deck[2];

				var result = RootNode.Cfr(cards, 1, 1);
				util += result;
				progress = (i * 100.0) / numIterations;
				
				if (i - lastUpdate > screenUpdateRange)
				{
					var newTime = DateTime.Now;
					var timeSpent = newTime - time;
					time = newTime;

					lastUpdate = i;
					Console.Clear();
					Console.WriteLine($"{progress}% {timeSpent.TotalSeconds}sec/{screenUpdateRange} iterations. Avg game value: {util / i}, Exploitability: {exploitability}");
				}

				if (i - lastExploitabilityUpdate > exploitabilityUpdateRange)
				{
					exploitability = RootNode.CalculateExploitability();
					lastExploitabilityUpdate = i;
				}
			}

			Console.WriteLine($"Average game value: {util / numIterations}");
			
		}

		public void TrainPCS(int numIterations)
		{
			var util = new double[Deck.Length];
			var lastUpdate = 0;
			var lastExploitabilityUpdate = 0;
			var progress = 0.0;
			var time = DateTime.Now;
			var totalCount = 0;
			var exploitability = 0.0;
			var screenUpdateRange = numIterations / 1000;
			var exploitabilityUpdateRange = numIterations / 100;

			var deck = Deck.ToArray();
			for (var i = 0; i < numIterations; i++)
			{
				for (var p = 0; p < 2; p++)
				{
					var reachProbabilities = new double[2, deck.Length];
					for (var j = 0; j < deck.Length; j++)
					{
						reachProbabilities[0, j] = 1;
						reachProbabilities[1, j] = 1;
					}

					var board = deck[Rng.Next(deck.Length)];
					var result = RootNode.PcsCfr(p, reachProbabilities, deck, board);
					
					progress = (i * 100.0) / numIterations;
				}

				if (i - lastUpdate > screenUpdateRange)
				{
					var newTime = DateTime.Now;
					var timeSpent = newTime - time;
					time = newTime;

					lastUpdate = i;
					Console.Clear();
					Console.WriteLine($"{progress}% {timeSpent.TotalSeconds}sec/{screenUpdateRange} iterations. Exploitability: {exploitability}");
				}

				if (i - lastExploitabilityUpdate > exploitabilityUpdateRange)
				{
					exploitability = RootNode.CalculateExploitability();
					lastExploitabilityUpdate = i;
				}
			}
		}

		public void TrainCfrPlus(int numIterations)
		{
			var lastUpdate = 0;
			var lastExploitabilityUpdate = 0;
			var progress = 0.0;
			var time = DateTime.Now;
			var totalCount = 0;
			var exploitability = 0.0;
			var screenUpdateRange = numIterations / 1000;
			var exploitabilityUpdateRange = numIterations / 100;

			var deck = Deck.ToArray();
			var d = 5;
			for (var i = 0; i < numIterations; i++)
			{
				var weight = Math.Max(i - d, 0);
				var reachProbabilities = new double[deck.Length];
				for (var j = 0; j < deck.Length; j++)
				{
					reachProbabilities[j] = 1;
				}

				for (var p = 0; p < 2; p++)
				{
					var result = RootNode.CfrPlus(p, weight, reachProbabilities, deck);

					progress = (i * 100.0) / numIterations;
				}

				if (i - lastUpdate > screenUpdateRange)
				{
					var newTime = DateTime.Now;
					var timeSpent = newTime - time;
					time = newTime;

					lastUpdate = i;
					Console.Clear();
					Console.WriteLine($"{progress}% {timeSpent.TotalSeconds}sec/{screenUpdateRange} iterations. Exploitability: {exploitability}");
				}

				if (i - lastExploitabilityUpdate > exploitabilityUpdateRange)
				{
					exploitability = RootNode.CalculateExploitability();
					lastExploitabilityUpdate = i;
				}
			}
		}

		public void Save(string fileName)
		{
			LeducNode.Save(RootNode, fileName);
		}

		public void Load(string fileName)
		{
			RootNode = LeducNode.Load(fileName);
		}

		private void ShuffleCards()
		{
			for (var c1 = Deck.Length - 1; c1 > 0; c1--)
			{
				var c2 = Rng.Next(c1 + 1);
				var tmp = Deck[c1];
				Deck[c1] = Deck[c2];
				Deck[c2] = tmp;
			}
		}
	}
}
