﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeducHoldemSolver
{
	public class LeducDealer
	{
		private Random _rng = new Random();
		private Card[] _deck = Card.EnumerateLeducDeck().ToArray();
		private LeducState _state = new LeducState();
		private List<LeducState> _nextStates = new List<LeducState>();
		private ILeducPlayer[] _players = new ILeducPlayer[2];

		public int CurrentPlayer => _state.GetAggregateState().CurrentPlayer;
		public IEnumerable<PlayerAction> AvailableActions => _nextStates.Select(s => s.GetLastAction());
		public bool IsGameFinished => _state.GetAggregateState().IsTerminal;

		public LeducDealer(ILeducPlayer player0, ILeducPlayer player1)
		{
			ShuffleDeck();
			_players[0] = player0;
			_players[1] = player1;

			for (var i = 0; i < _players.Length; i++)
			{
				var playerIndex = i;
				_players[playerIndex].SetCard(_deck[playerIndex]);
				_players[playerIndex].PlayerActingEvent += (sender, args) => Act(playerIndex, args.Action);
			}
		}

		public void StartGame()
		{
			SetState(new LeducState());
		}

		public void Act(int player, PlayerAction action)
		{
			if (player != CurrentPlayer)
			{
				throw new ArgumentException("Incorrect player");
			}

			if (!AvailableActions.Any(a => a.Equals(action)))
			{
				throw new ArgumentException("Invalid action");
			}

			SetState(_nextStates.Find(s => s.GetLastAction().Equals(action)));
		}

		public Card GetCard(int player)
		{
			if (player > 1)
			{
				throw new ArgumentException("Invalid player");
			}

			return _deck[player];
		}

		private void SetState(LeducState state)
		{
			_state = state;
			_nextStates = _state.Expand().Cast<LeducState>().ToList();

			if (_nextStates.Count == 0)
			{
				var utility = _state.GetTerminalUtility(_deck);
				var currentPlayer = _state.GetAggregateState().CurrentPlayer;
				_players[currentPlayer].SetUtility(utility.Value);
				_players[1 - currentPlayer].SetUtility(-utility.Value);
			}
			else if (_state.GetAggregateState().IsChanceNode)
			{
				SetState(_nextStates.Find(s => s.Board.Equals(_deck[2])));
			}
			else
			{
				_players[_state.GetAggregateState().CurrentPlayer].SetState(_state, AvailableActions);
			}
		}
	

		private void ShuffleDeck()
		{
			for (var c1 = _deck.Length - 1; c1 > 0; c1--)
			{
				var c2 = _rng.Next(c1 + 1);
				var tmp = _deck[c1];
				_deck[c1] = _deck[c2];
				_deck[c2] = tmp;
			}
		}
	}
}
