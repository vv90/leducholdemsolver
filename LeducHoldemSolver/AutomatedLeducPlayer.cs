﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeducHoldemSolver
{
	public class AutomatedLeducPlayer : ILeducPlayer
	{
		private Card _card;
		private LeducNode _rootNode;
		private Random _rng = new Random();

		public int UtilityGain { get; private set; }
		public event EventHandler<PlayerActingEventArgs> PlayerActingEvent;

		public AutomatedLeducPlayer(LeducNode rootNode)
		{
			_rootNode = rootNode;
			UtilityGain = 0;
		}

		public void SetCard(Card card)
		{
			_card = card;
		}

		public void SetState(LeducState state, IEnumerable<PlayerAction> possibleActions)
		{
			var strategy = _rootNode.LookupStrategy(state, _card);
			var chance = _rng.NextDouble();
			var chanceSum = 0.0;
			PlayerAction selectedAction = new PlayerAction();
			foreach (var item in strategy)
			{
				chanceSum += item.Value;
				if (chanceSum > chance)
				{
					selectedAction = item.Key;
					break;
				}
			}

			PlayerActingEvent?.Invoke(this, new PlayerActingEventArgs(selectedAction));
		}

		public void SetUtility(int amount)
		{
			UtilityGain += amount;
		}

		
	}

	public class StaticLeducPlayer : ILeducPlayer
	{
		private Random _rng = new Random();
	
		public event EventHandler<PlayerActingEventArgs> PlayerActingEvent;
		public int UtilityGain { get; private set; }

		public void SetCard(Card card)
		{
			
		}

		public void SetState(LeducState state, IEnumerable<PlayerAction> possibleActions)
		{
			var actions = possibleActions.ToArray();
			var selectedAction = actions[_rng.Next(actions.Length)];
			
			PlayerActingEvent?.Invoke(this, new PlayerActingEventArgs(selectedAction));
		}

		public void SetUtility(int amount)
		{
			UtilityGain += amount;
		}
	}
}
