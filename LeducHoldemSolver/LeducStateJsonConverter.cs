﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LeducHoldemSolver
{
	public class LeducStateJsonConverter : JsonConverter<LeducState>
	{
		public override void WriteJson(JsonWriter writer, LeducState value, JsonSerializer serializer)
		{
			writer.WriteValue(value.ToString());
		}

		public override LeducState ReadJson(JsonReader reader, Type objectType, LeducState existingValue, bool hasExistingValue,
			JsonSerializer serializer)
		{
			return LeducState.Parse((string) reader.Value);
		}
	}
}
