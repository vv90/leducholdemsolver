﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeducHoldemSolver
{
	public class AggregateState
	{
		public int[] Bets = new int[2] { 100, 100 };
		public bool[] PlayerActed = new bool[2] { false, false };
		public bool[] PlayerPlaying = new bool[2] { true, true };
		public int CommonPot = 0;
		public int CurrentPlayer = 0;
		public bool IsTerminal = false;
		public bool IsChanceNode = false;
		public int NumActions = 0;

		public void FinishBetRound()
		{
			CommonPot = Bets[0] + Bets[1];
			Bets[0] = Bets[1] = 0;
			PlayerActed[0] = PlayerActed[1] = false;
			NumActions = 0;
			CurrentPlayer = 0;
		}

		public bool BetRoundFinished()
		{
			return PlayerActed.All(p => p) && Bets[0] == Bets[1];
		}
	}
}
