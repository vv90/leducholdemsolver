﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LeducHoldemSolver
{
	public class CardJsonConverter : JsonConverter<Card>
	{
		public override void WriteJson(JsonWriter writer, Card value, JsonSerializer serializer)
		{
			writer.WriteValue(value.ToString());
		}

		public override Card ReadJson(JsonReader reader, Type objectType, Card existingValue, bool hasExistingValue, JsonSerializer serializer)
		{
			var cardStr = (string) reader.Value;
			var success = Card.TryParse(cardStr, out var card);

			if (!success)
			{
				throw new Exception($"Unable to parse card {cardStr}");
			}

			return card;
		}
	}
}
